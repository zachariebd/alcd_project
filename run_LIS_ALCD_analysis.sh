#!/bin/bash


module load lis/1.6
module load amalthee

echo "start LIS_ALCD_analysis.py"
python /work/OT/siaa/Theia/Neige/CoSIMS/zacharie/TOOLS/ALCD_validation/LIS_ALCD_analysis.py   -REF_path "/work/OT/siaa/Theia/Neige/CoSIMS/zacharie/TOOLS/ALCD_validation/ALCD"\
                                                                                              -Output_path "/work/OT/siaa/Theia/Neige/CoSIMS/zacharie/TOOLS/ALCD_validation/results"\
                                                                                              -run_LIS 1\
                                                                                              -run_VAL 1\
                                                                                              -nb_steps 1\
                                                                                              -ndsi_step 0.01\
                                                                                              -red_step 10\
                                                                                              -direction 0\
                                                                                              -date1 "20190101"\
                                                                                              -date2 "20200601"\
                                                                                              -tile "T31TCH"\
                                                                                              -results 2\
                                                                                              -keep_LIS 0\
                                                                                              -max_J 100\
                                                                                              -red1_init 200\
                                                                                              -red2_init 40\
                                                                                              -ndsi1_init 0.4\
                                                                                              -ndsi2_init 0.15\
    

echo "end LIS_ALCD_analysis.py"




















































































































