import os
import sys
import errno
import re
import glob
import os.path as op
import json
import csv
import copy
import logging
import subprocess
from datetime import datetime, timedelta
from libamalthee import Amalthee
import argparse
from shutil import copyfile



def mkdir_p(dos):
    try:
        os.makedirs(dos)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(dos):
            pass
        else:
            raise
            
def getDateFromStr(N):
    sepList = ["","-","_","/"]
    date = ''
    for s in sepList :
        found = re.search('\d{4}'+ s +'\d{2}'+ s +'\d{2}', N)
        if found != None :
           date = datetime.strptime(found.group(0), '%Y'+ s +'%m'+ s +'%d').date()
           break
    return date
    
def getListDateDecal(start_d,end_d,directory,decal):
    lo=[]
    li = []
    
    try:
        li = os.listdir(directory)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EACCES:
            return lo
        else:
            raise       
    
    for i in sorted(li):
        date = getDateFromStr(i) 
        if date == '' : continue
        if (date >= getDateFromStr(start_d) - timedelta(days = decal)) and (date <= getDateFromStr(end_d) + timedelta(days = decal)) :
            lo.append(os.path.join(directory,i))
    return lo
    
def call_subprocess(process_list):
    """ Run subprocess and write to stdout and stderr
    """
    logging.info("Running: " + " ".join(process_list))
    process = subprocess.Popen(
        process_list,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE)
    out, err = process.communicate()
    
    logging.info(out)
    sys.stderr.write(str(err))
    return out
    

def run_amalthee(list_L2A_file,REF_path,date1,date2,tile):
    
    
    
    FileOut = open(list_L2A_file,"w")
    
    list_REF_products_path = getListDateDecal(date1,date2,REF_path,0)
    
    
    for i in list_REF_products_path:
        
        date = getDateFromStr(i)
    
            
        parameters = {"processingLevel": "LEVEL2A", "location": tile}
        amalthee_theia = Amalthee('theia')
        amalthee_theia.search("SENTINEL2",
                                  date.strftime("%Y-%m-%d"),
                                  (date + timedelta(days=1)).strftime("%Y-%m-%d"),
                                  parameters,
                                  nthreads = 6)
                                  
        nb_products = amalthee_theia.products.shape[0]
        print (date.strftime("%Y-%m-%d")+"T00:00:00")
        df = amalthee_theia.products
        for product_id in df.index:
            print('Processing ' + product_id)
            # check datalake availability
            if df.loc[product_id, 'available']:
                print('filling datalake')
                amalthee_theia.fill_datalake()
                print('filling L2A list for LIS production')
                FileOut.write(df.loc[product_id, 'datalake']+"\n")
            else: print(product_id + ' not available')
    
    FileOut.close()



def make_combinations(direction,red_step,ndsi_step,red1_init,red2_init,ndsi1_init,ndsi2_init,nb_steps,list_L2A_file):
    
    
    
    dict_params = {"red_pass1":[red1_init],
                   "red_pass2":[red2_init],
                   "ndsi_pass1":[ndsi1_init],
                   "ndsi_pass2":[ndsi2_init]}
               
    print("nb_steps ",nb_steps)

    
    if nb_steps > 0:
        for param in dict_params:
            if "red" in param: step = red_step
            elif "ndsi" in param: step = ndsi_step
            for i in range(1,nb_steps+1,1):
                if direction == 0 or direction == 2:
                    new_value = round(dict_params[param][0]  + i*step,3)
                    dict_params[param].append(new_value)
                if direction == 1 or direction == 2:
                    new_value = round(dict_params[param][0]  - i*step,3)
                    if new_value >= 0 : dict_params[param].append(new_value)
            print("number of values for "+param+" : "+str(len(dict_params[param])))
    

    # comb_params = [[]]
    # for _, vals in sorted(dict_params.items()):
        # comb_params = [x+[y] for x in comb_params for y in vals]
        
    
    list_L2A = [x.strip() for x in open(list_L2A_file,"r").readlines()]
    print("number of dates :"+str(len(list_L2A)))
    dict_params["L2A"] = list_L2A
    comb_all = [[]]
    for _, vals in sorted(dict_params.items()):
        comb_all = [x+[y] for x in comb_all for y in vals]

    print("number of calculated combinations : " + str(len(comb_all)))
    
    
    return sorted(comb_all)


    
    
    
    
    
    
    






parser = argparse.ArgumentParser()

parser.add_argument('-REF_path', action='store', default="", dest='REF_path', help='where reference products can be found')
parser.add_argument('-Output_path', action='store', default="", dest='Output_path', help='where results are saved')
parser.add_argument('-run_LIS', action='store', type=int,default=1, dest='run_LIS', help='calculate LIS products from the L2A THEIA products in list_L2A_"tile".txt')
parser.add_argument('-run_VAL', action='store', type=int,default=1, dest='run_VAL', help='calculate the kappa between the LIS products and the reference products')
parser.add_argument('-nb_steps', action='store', type=int,default=1, dest='nb_steps', help='number of different value for each parameters')
parser.add_argument('-red_step', action='store', type=float,default=10, dest='red_step', help='value substracted from or added to original red band limit value')
parser.add_argument('-ndsi_step', action='store', type=float,default=0.01, dest='ndsi_step', help='value substracted from or added to original ndsi limit value')
parser.add_argument('-direction', action='store', type=int,default=0, dest='direction', help='decide if we substract, add or both (0:sub;1:add;2:both)')
parser.add_argument('-date1', action='store',default="19900101", dest='date1', help='starting date in the YYYYMMDD format')
parser.add_argument('-date2', action='store', default="30000101", dest='date2', help='ending date in the YYYYMMDD format')
parser.add_argument('-tile', action='store', default="", dest='tile', help='S2 tile (ex: T31TCH)')
parser.add_argument('-results', action='store', type=int,default=0, dest='results', help='0: all combinations are processed and result file is overwritten; 1: all combinations are processed and result file is updated; 2: combinations not found in result file are processed and result file is updated')
parser.add_argument('-keep_LIS', action='store', type=int,default=0, dest='keep_LIS', help='keep LIS products at the end of the job (always false when more than 50 combinations)')
parser.add_argument('-max_J', action='store', type=int,default=1000, dest='max_J', help='maximum number of parallel jobs')
parser.add_argument('-red1_init', action='store', type=float,default=200, dest='red1_init', help='initial value of the red band pass 1')
parser.add_argument('-red2_init', action='store', type=float,default=40, dest='red2_init', help='initial value of the red band pass 2')
parser.add_argument('-ndsi1_init', action='store', type=float,default=0.4, dest='ndsi1_init', help='initial value of the ndsi band pass 1')
parser.add_argument('-ndsi2_init', action='store', type=float,default=0.15, dest='ndsi2_init', help='initial value of the ndsi band pass 2')



REF_path = parser.parse_args().REF_path
Output_path = parser.parse_args().Output_path
run_LIS = parser.parse_args().run_LIS
run_VAL = parser.parse_args().run_VAL
nb_steps = parser.parse_args().nb_steps
red_step = parser.parse_args().red_step
ndsi_step = parser.parse_args().ndsi_step
red1_init = parser.parse_args().red1_init
ndsi1_init = parser.parse_args().ndsi1_init
red2_init = parser.parse_args().red2_init
ndsi2_init = parser.parse_args().ndsi2_init
direction = parser.parse_args().direction
date1 = parser.parse_args().date1
date2 = parser.parse_args().date2
tile = parser.parse_args().tile
keep_LIS = parser.parse_args().keep_LIS
results = parser.parse_args().results
max_J = parser.parse_args().max_J



REF_path = op.join(REF_path,tile)

Output_path = op.join(Output_path,tile)

mkdir_p(Output_path)



list_L2A_file = op.join(Output_path,"list_L2A.txt")
list_comb_file = op.join(Output_path,"list_comb.txt")
results_file = op.join(Output_path,"results_days.txt")

print("search for L2A products from THEIA with amalthee")
run_amalthee(list_L2A_file,REF_path,date1,date2,tile)

print("making a list of all possible combinations")
comb_all = make_combinations(direction,red_step,ndsi_step,red1_init,red2_init,ndsi1_init,ndsi2_init,nb_steps,list_L2A_file)




if results == 0:
    print("results_"+tile+".txt will be overwritten")
    FileNewResults = open(results_file, 'w')
    FileNewResults.write('L2A date n1 n2 r1 r2 KAPPA cm11 cm12 cm13 cm21 cm22 cm23 cm31 cm32 cm33\n')
    FileNewResults.close()
if results == 1:
    print("previous results from existing combination in result file will be updated, results from new combinations will be added")
    FilePreviousResults = open(results_file,"r")
    ListPreviousResults = FilePreviousResults.readlines()[1:]
    FilePreviousResults.close()
    FileNewResults = open(results_file,"w")
    for LinePreviousResults in ListPreviousResults:
        params = LinePreviousResults.split()
        date = params[1]
        n1 = float(params[2])
        n2 = float(params[3])
        r1 = float(params[4])
        r2 = float(params[5])
        presence = 0
        for comb in comb_all:
            if getDateFromStr(comb[0]).strftime("%Y%m%d") == date and comb[1] == n1 and comb[2] == n2 and comb[3] == r1 and comb[4] == r2: presence = 1
        if presence == 0:
            FileNewResults.write(LinePreviousResults)
    FileNewResults.close()
                
if results == 2:
    #i = 0
    print("only results from new combinations will be added to result file")
    with open(results_file,"r") as FilePreviousResults:
        next(FilePreviousResults)
        for LinePreviousResults in FilePreviousResults:
            params = LinePreviousResults.split()
            L2A = params[0]
            n1 = float(params[2])
            n2 = float(params[3])
            r1 = float(params[4])
            r2 = float(params[5])
            #i = i + 1
            #print("line : ",i)
            try:
                comb_all.remove([L2A,n1,n2,r1,r2])
                #print("combination removed")
            except ValueError:
                #print("combination not removed")
                pass  # do nothing!
            
            # for comb in comb_all:
                # #print("check comb : ",getDateFromStr(comb[0]).strftime("%Y%m%d"),comb[1],comb[2],comb[3],comb[4])
                # if getDateFromStr(comb[0]).strftime("%Y%m%d") == date and comb[1] == n1 and comb[2] == n2 and comb[3] == r1 and comb[4] == r2:
                    # comb_all.remove(comb)
                    # break

print("number of possible combinaisons = ",len(comb_all))

print("write all combinations to process in file ",list_comb_file)

FileAllComb = open(list_comb_file,"w")
FileAllComb.write("L2A n1 n2 r1 r2")
for i in comb_all :
    FileAllComb.write("\n") 
    for j in i:
        FileAllComb.write(str(j)+" ") 
FileAllComb.close()


if run_LIS == 1 or run_VAL == 1:

    print("organizing job arrays:")
    nb_jobs = len(comb_all)
    print("    number of possible combinaisons = nb of jobs to do =",nb_jobs)
    if nb_jobs  == 0:
        print("    no jobs to do, end of process")
        exit()
    elif nb_jobs ==1:
        print("    only one job to do")
    else:
        print("    jobs will run in groups of ",max_J)
        if nb_jobs > 50 : 
            keep_LIS= False
            print("    this will generate too many LIS products (>50), they will not be saved")
        
    
    
    nb_job_array = float(nb_jobs)/float(max_J)
    int_jobs = int(nb_job_array // 1) + 1
    job_id = ""
    command_A = ["qsub",
                       "-k",
                       "n",
                       "-v",
                       "list_comb_file=\""+list_comb_file+"\",REF_path=\""+REF_path+"\",Output_path=\""+Output_path+"\",run_LIS=\""+str(run_LIS)+"\",run_VAL=\""+str(run_VAL)+"\",tile=\""+tile+"\",keep_LIS=\""+str(keep_LIS)+"\""]
    
    
    
    
    
    print("starting sending jobs")
    for i in range(0,int_jobs):
        command_Out = []
        command_J = []
        
        if i == 0 : command_Out = ["run_LIS_comp_ALCD.sh"]
        else : command_Out = ["-W", "depend=afterany:"+job_id ,"run_LIS_comp_ALCD.sh"]
           
           
        command_J = command_A + command_Out
        
        
        if nb_jobs == 1:
            print("sending one job")
        else :
            job_array = ""
            a = 1 + (i)*max_J
            b = (i+1)*max_J
            if b > nb_jobs : job_array = str(a)+"-"+str(nb_jobs)
            else: job_array = str(a)+"-"+str(b)
            print("sending jobs ", job_array)
            command_J.insert(1, "-J")
            command_J.insert(2, job_array )
        
        #print(" ".join(command_J))
        job_id = call_subprocess(command_J)
        job_id = str( job_id ).split("\'")[1].split(".")[0]
        
    
        command_clean = ["qsub",
                         "-W", 
                         "depend=afterany:"+job_id,
                         "clean_hpc_logs.sh"]
    
        job_id = call_subprocess(command_clean)
        job_id = str( job_id ).split("\'")[1].split(".")[0]
