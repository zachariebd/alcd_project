#!/bin/bash
#PBS -N do_LIS
#PBS -j oe
#PBS -l select=1:ncpus=4:mem=20000mb
#PBS -l walltime=00:59:00
#PBS -m e
#PBS -k n


# Store the directory in  PBS_TMP_DIR

PBS_TMPDIR=$TMPDIR
# Unset TMPDIR env variable which conflicts with openmpi
unset TMPDIR




module load lis/1.6




# list_comb_file to process
if [ -z $list_comb_file ]; then
  echo "list_comb_file is not set, exit"
  exit 1
fi
echo "combination file: " +$list_comb_file

if [ -z $tile ]; then
  echo "tile is not set, exit"
  exit 1
fi
echo "tile: " $tile



if [ -z $Output_path ]; then
  echo "Output_path is not set, exit"
  exit 1
fi
echo "output path : "$Output_path

if [ -z $run_LIS ]; then
  echo "run_LIS parameter is not set. default value is 1"
  run_LIS="1"
fi

if [ -z $keep_LIS ]; then
  echo "keep_LIS parameter is not set. default value is 0"
  keep_LIS="0"
fi

if [ -z $run_VAL ]; then
  echo "run_VAL parameter is not set. default value is 1"
  run_VAL="1"
fi

if [ -z $REF_path ]; then
  echo "REF_path is not set. exit"
  exit 1
fi
echo "reference products path: " $REF_path


readarray -t array_products < $list_comb_file
# use the PBS_ARRAY_INDEX variable to distribute jobs in parallel (bash indexing is zero-based)
line=""
echo "PBS_ARRAY_INDEX IS " ${PBS_ARRAY_INDEX}
if [ -z $PBS_ARRAY_INDEX ]; then
    line="${array_products[1]}"
else
    line="${array_products[${PBS_ARRAY_INDEX}]}"
fi

#line="${array_products[0]}"
echo "line = " $line
params=($line)
L2A=${params[0]}
n1=${params[1]}
n2=${params[2]}
r1=${params[3]}
r2=${params[4]}


echo "L2A = " $L2A
echo "n1 = " $n1
echo "n2 = " $n2
echo "r1 = " $r1
echo "r2 = " $r2

L2A_product=$(basename ${L2A})
comb_name=JOB_${PBS_ARRAY_INDEX}

# working directory
tmp_LIS_dir=$PBS_TMPDIR/LIS/$comb_name
tmp_L2A_dir=$PBS_TMPDIR/L2A/$comb_name


mkdir -p $tmp_LIS_dir
mkdir -p $tmp_L2A_dir

# storage directory
#storage_output_dir=$out_path/$tile/$comb_name
#mkdir -p $storage_output_dir

#path_input_comp=$out_path/$tile/$comb_name/$(basename ${L2A})



if [ $run_LIS == "1" ]; then

    # DEM input path
    pdem="/work/OT/siaa/Theia/Neige/DEM/"
    pdem_l8="/work/OT/siaa/Theia/Neige/DEM_L8/"
    inputdem=$(find $pdem/S2__TEST_AUX_REFDE2_${tile}_0001.DBL.DIR/ -name "*ALT_R2.TIF")
    echo "inputdem:" $inputdem
    inputdem_l8=$(find $pdem_l8/L8_TEST_AUX_REFDE2_${tile}_0001.DBL.DIR/ -name "*ALT.TIF")
    echo "inputdem l8:" $inputdem_l8
    
    
    if [ -z $L2A ]; then
      echo "No L2A product to process PBS_ARRAY_INDEX:" ${PBS_ARRAY_INDEX}
      exit 0
    fi
    
    
    # use the product name to identify the config and output files
    #product_folder=$(basename $L2A)



    
    #copy input data
    cp -r $L2A ${tmp_L2A_dir}
    cd ${tmp_L2A_dir}

    
    img_input=$tmp_L2A_dir/$L2A_product

    
    # create working output directory
    pout=$tmp_LIS_dir/$L2A_product
    mkdir -p $pout
    echo "The output folder within working directory is " $pout
    
    
    
    #configure gdal_cachemax to speedup gdal polygonize and gdal rasterize (half of requested RAM)
    export GDAL_CACHEMAX=2048
    echo $GDAL_CACHEMAX
    
    #check if L8 products to use the correct DEM
    if [[ $L2A_product == *LANDSAT8* ]];
    then
        inputdem=$inputdem_l8
        echo 'using dem $inputdem'
    fi
    
    # create config file
    date ; echo "START build_json.py $config"
    build_json.py -ram 2048 -dem $inputdem -red_pass1 $r1 -red_pass2 $r2 -ndsi_pass1 $n1 -ndsi_pass2 $n2 $img_input $pout
    config=${pout}/param_test.json
    echo $config
    date ; echo "END build_json.py"
    
    if [ -z "$(ls -A -- $pout)" ]
     then
        echo "ERROR: $pout directory is empty, something wrong happen during build_json"
        exit 1
    fi
    
    # run the snow detection
    date ; echo "START run_snow_detector.py $config"
    run_snow_detector.py $config
    date ; echo "END run_snow_detector.py"
    
    if [ -z "$(ls -A -- ${pout}/LIS_PRODUCTS)" ]
     then
        echo "ERROR: ${pout}/LIS_PRODUCTS directory is empty, something wrong happen during run_snow_detector"
        exit 1
    fi
    
    
    # copy output to /work
    #mkdir -p $storage_output_dir
    #cp -r $pout $storage_output_dir/
    #final_path=$storage_output_dir/
    #chgrp -R lis_admin $final_path
    #chmod 775 -R $final_path
    #echo "Product is available under $final_path"
    rm -rf $tmp_L2A_dir

    
fi




if [ $run_VAL == "1" -a $run_LIS == "1" ]; then
    
    

    input_LIS=$tmp_LIS_dir/$L2A_product
    input_REF=$REF_path
    output_tmp_result=$tmp_LIS_dir/tmp_result.txt
    result_file_to_append=$Output_path/results_days.txt

    echo "START compar_ALCD_LIS.py -ref $input_REF -lis $input_LIS -o $output_tmp_result"
    python /work/OT/siaa/Theia/Neige/CoSIMS/zacharie/TOOLS/ALCD_validation/compar_ALCD_LIS.py -ref $input_REF -lis $input_LIS -o $output_tmp_result
    
    echo "END compar_ALCD_LIS.py"
    readarray -t array_tmp_results < $output_tmp_result
    
    line="${array_tmp_results[0]}"
    echo "line : "$line
    results=($line)
    date=${results[0]}
    rest=${results[@]:1}
    echo "$L2A $date $n1 $n2 $r1 $r2 $rest" >> $result_file_to_append
    
    
    
    

fi



if [ $keep_LIS == "1" ]; then

    saved_LIS_dir=$Output_path/LIS/${n1}_${n2}_${r1}_${r2}
    mkdir -p $saved_LIS_dir
    cp -r $tmp_LIS_dir/$L2A_product $saved_LIS_dir/

else

    rm -rf $tmp_LIS_dir/$L2A_product
fi
