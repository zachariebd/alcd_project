ANACONDA3


ENVIRONMENTS: base (python 3.8); py2 (python 2.7)



How to install OTB6.6.1 (for python 2.7)###########################################
prerequiste:  sudo apt-get install libexpat-dev or libexpat1-dev
conda activate py2 to set python2 environment
conda install numpy (or pip install)
download OTB-6.6.1-Linux64.run from https://www.orfeo-toolbox.org/packages/archives/OTB/ 
move it in a directory of your choosing, chmod +x it and run it (./OTB-6.6.1-Linux64.run) to install otb6.6.1
if there is a error message about a file it cannot find, apt get it and redo the installation

How to run OTB6.6.1 (for python2.7)#########################################################
source otbenv.profile found in OTB-6.6.1-Linux64
conda activate py2
open python (python command or spyder..)
import numpy
import otbApplication
numpy must be imported before otbApplication

How to automaticaly source otbenv.profile when activating py2###########################################
Dans anaconda3/envs/py2/etc, avoir un repertoire conda avec a l'interieur deux repertoires activate.d et deactivate.d
creer les fichiers activate.d/env_vars.sh et deactivate.d/env_vars.sh
ils contiennent des scripts joués a chaque activation/desactivation de py2
dans activate.d/env_vars.sh mettre "source /home/zacharie/OTB6/OTB-6.6.1-Linux64/otbenv.profile "
pour sourcer l'environement necessaire a OTB6.6.1

How to install gdal/ogr in python 2.7 env######################################################"
conda activate py2
sudo add-apt-repository ppa:ubuntugis/ppa && sudo apt-get update (not 100% sure its necessary)
sudo apt-get update
sudo apt-get install gdal-bin
sudo apt-get install libgdal-dev
export CPLUS_INCLUDE_PATH=/usr/include/gdal (add it in activate.d/env_vars.sh)
export C_INCLUDE_PATH=/usr/include/gdal (add it in activate.d/env_vars.sh)
ogrinfo --version
pip install GDAL==<gdal version from ogrinfo>

now its possible to import osgeo, org, gdal



