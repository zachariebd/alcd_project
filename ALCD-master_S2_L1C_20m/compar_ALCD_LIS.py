import sys
import os
import errno
import re
import math
from datetime import datetime, timedelta, date
from osgeo import osr, gdal
from osgeo.gdalnumeric import *
from osgeo.gdalconst import *
import numpy as np
import matplotlib
#matplotlib.use('Agg')
import matplotlib.pyplot as plt
import scipy.optimize as opti
from scipy.stats import mstats
import shutil
import argparse
import matplotlib.gridspec as gridspec
from matplotlib.colors import LogNorm
from pyproj import Proj, transform
import glob
import random
import pandas as pd
from sklearn import datasets, linear_model
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
from sklearn.metrics import cohen_kappa_score
from math import sqrt
from matplotlib.ticker import PercentFormatter
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from sklearn.metrics import f1_score
from sklearn.metrics import accuracy_score
import csv
import json
from pprint import pprint



def mkdir_p(dos):
    try:
        os.makedirs(dos)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(dos):
            pass
        else:
            raise
            
def getDateFromStr(N):
    sepList = ["","-","_","/"]
    date = ''
    for s in sepList :
        found = re.search('\d{4}'+ s +'\d{2}'+ s +'\d{2}', N)
        if found != None :
           date = datetime.strptime(found.group(0), '%Y'+ s +'%m'+ s +'%d').date()
           break
    return date





def matrix_loading(confusion_matrix_path):
    ''' Loads the matrix given its path, and returns
    the confusion matrix along with the classes names
    '''

    # loads the data
    datafile = open(confusion_matrix_path, 'r')
    lines = datafile.readlines()
    datafile.close()
    lines = [l.replace('\n', '') for l in lines]

    headers = lines[0:1]
    data = lines[1:]

    # Get the references classes
    classes_ref = headers[0].split(',')
    classes_ref = [int(c) for c in classes_ref]

    # Get the produced classes
    classes_produced = headers[0].split(',')
    classes_produced = [int(c) for c in classes_produced]

    print(classes_ref)
    print(classes_produced)

    # Get the data and shapes the array, while converting to int
    confusion_matrix_original = []
    for row in data:
        confusion_matrix_original.append([int(r) for r in row.split(',')])

    # To make the confusion matrix square, i.e. if some classes are
    # missing in the referenced or produced data :
    classes_all = list(set(classes_ref + classes_produced))
    nb_class = len(classes_all)

    print('Classes: {}'.format(classes_all))

    conf_mat = np.zeros((nb_class, nb_class), float)

    for i in range(nb_class):
        for j in range(nb_class):
            I = None
            J = None
            try:
                I = classes_ref.index(classes_all[i])
                J = classes_produced.index(classes_all[j])
            except:
                I = None
                J = None
            if I != None and J != None:
                conf_mat[i][j] = confusion_matrix_original[I][J]

    print('Confusion matrix:')
    pprint(conf_mat)

    return classes_all, conf_mat




def multi_to_binary_confusion_matrix(a_classes,b_classes,confusion_matrix_path):
    ''' Transform a multiclass confusion matrix to a binary one,
    allowing the computation of metrics such as accuracy or F1 score
    '''


    classes, cm = matrix_loading(confusion_matrix_path)
    nb_classes = len(classes)

    FNs = []
    FPs = []
    TPs = []
    TNs = []

    for i in range(nb_classes):
        for j in range(nb_classes):
            if classes[i] in a_classes and classes[j] in a_classes:
                TPs.append(cm[i][j])
            if classes[i] in b_classes and classes[j] in b_classes:
                TNs.append(cm[i][j])
            if classes[i] in a_classes and classes[j] in b_classes:
                FNs.append(cm[i][j])
            if classes[i] in b_classes and classes[j] in a_classes:
                FPs.append(cm[i][j])

    TN = float(sum(TNs))
    TP = float(sum(TPs))
    FP = float(sum(FPs))
    FN = float(sum(FNs))

    print('Binary confusion matrix:')
    print('{:10}'.format(TP) + ' | ' + '{:10}'.format(FP) +
          '\n --------------------\n' + '{:10}'.format(FN) + ' | ' + '{:10}'.format(TN))

    bin_conf_mat = [[TP, FN], [FP, TN]]

    return bin_conf_mat





def binary_stats(bin_conf_mat, all_stats=False):
    ''' Compute the metrics from a binary confusion matrix
    '''

    TP = bin_conf_mat[0][0]  # true positive
    FN = bin_conf_mat[0][1]  # false negative
    FP = bin_conf_mat[1][0]  # false positive
    TN = bin_conf_mat[1][1]  # true negative

    accuracy = (TP+TN)/(TP+TN+FP+FN)
    if TP+FP == 0:
        precision = 1
    else:
        precision = (TP)/(TP+FP)
    if TP+FN == 0:
        recall = 1
    else:
        recall = (TP)/(TP+FN)
    if TN+FP == 0:
        specificity = 1
    else:
        specificity = (TN)/(TN+FP)
    f1= 2*(precision*recall)/(precision+recall)

    print('Accuracy: {:.2f} %'.format(accuracy*100))
    print('F1 Score: {:.2f} %'.format(f1*100))

    print('(Precision: {:.2f} %)'.format(precision*100))
    print('(Recall: {:.2f} %)'.format(recall*100))
    print('(Specificity: {:.2f} %)'.format(specificity*100))

    if all_stats == False:
        return accuracy, f1

    else:
        return accuracy, f1, precision, recall, specificity






#Arguments parsing
parser = argparse.ArgumentParser()
parser.add_argument('-d', action='store', default="", dest='date', help='date for comparison (e.g. 20170702)')
parser.add_argument('-l', action='store', default="", dest='location', help='location for comparison (e.g. PYR)')
parser.add_argument('-c', action='store', default=False, dest='combine', help='combine matrices and kappas from all dates')
date = parser.parse_args().date
location= parser.parse_args().location
combine = parser.parse_args().combine


path_data_ALCD = "/home/zacharie/ALCD-master_S2_L1C/Data_ALCD"
path_data_LIS= "/home/zacharie/ALCD-master_S2_L1C/LIS"
path_data_LIS_loc = os.path.join(path_data_LIS,location)

list_path_dates_ALCD = []
list_path_dates_LIS= []
if date == "" : date = "all"
print ("we list the valid dates")
for i in os.listdir(path_data_ALCD) :
    if (location in i) and ((date == "all") or (date in i)):
        d = getDateFromStr(i).strftime("%Y%m%d")
        for j in os.listdir(path_data_LIS_loc) :
            if d in j:
                list_path_dates_ALCD.append(os.path.join(path_data_ALCD,i))
                list_path_dates_LIS.append(os.path.join(path_data_LIS_loc,j))
                print (d)
                
#classes values for L1C ALCD product
ALCD_snow = 7
ALCD_water = 6
ALCD_lcloud = 2       
ALCD_land = 5
ALCD_hcloud = 3
ALCD_shadow = 4
ALCD_bckgrnd = 1
ALCD_nodata = 0

#classes values for L2A LIS SEB product
LIS_snow = 100
LIS_cloud = 205       
LIS_nodata = 254 #nodata aussi present dans L1C
LIS_land = 0



path_class_ALCD = ""
path_SEB_LIS = ""
path_red_LIS = ""
path_swir_LIS = ""
path_green_LIS = ""
path_comp = ""     

list_b_ALCD = []
list_b_SEB = []   
list_b_ALCD_snow = []
list_b_SEB_snow  = []  
list_b_ALCD_cloud = []
list_b_SEB_cloud  = []  
cm_tot = []
cm2_tot = []

for k in arange(len(list_path_dates_ALCD)):
    print ("processing " + getDateFromStr(list_path_dates_ALCD[k]).strftime("%Y%m%d"))
    path_class_ALCD = os.path.join(list_path_dates_ALCD[k],"Out/labeled_img_regular.tif")
    path_comp = os.path.join(list_path_dates_ALCD[k],"Comp")
    path_SEB_LIS = os.path.join(list_path_dates_LIS[k],"LIS_SEB.TIF")
    path_red_LIS = os.path.join(list_path_dates_LIS[k],"red_band_resampled.tif")
    path_green_LIS = os.path.join(list_path_dates_LIS[k],"green_band_resampled.tif")
    path_swir_LIS = os.path.join(list_path_dates_LIS[k],"swir_band_extracted.tif")
    
    mkdir_p(path_comp)
    
    #extraction of image data
    print("extraction of image data")
    g_ALCD = gdal.Open(path_class_ALCD)
    g_SEB = gdal.Open(path_SEB_LIS)
    g_red = gdal.Open(path_red_LIS)
    g_swir = gdal.Open(path_swir_LIS)
    g_green = gdal.Open(path_green_LIS)
    b_ALCD = BandReadAsArray( g_ALCD.GetRasterBand(1))
    b_SEB= BandReadAsArray( g_SEB.GetRasterBand(1))
    b_red = BandReadAsArray( g_red.GetRasterBand(1))
    b_swir = BandReadAsArray( g_swir.GetRasterBand(1))
    b_green = BandReadAsArray( g_green.GetRasterBand(1))
    
    g_green = None
    g_swir = None
    g_red = None
    g_SEB = None
    
    
    g_comp =   gdal.Translate('',g_ALCD,format= 'MEM')
    g_ALCD = None
    b_comp = np.zeros_like(b_ALCD)
     
    #nodata
    cond = np.where(b_SEB == LIS_nodata)
    b_ALCD[cond] = LIS_nodata
    cond = np.where(b_ALCD ==ALCD_nodata)
    b_ALCD[cond] = LIS_nodata
    b_SEB[cond] = LIS_nodata
    
    print("labelisation")
    # equivalent values to classes
    cond = np.where(b_ALCD == ALCD_snow) 
    b_ALCD[cond] = LIS_snow
    cond = np.where(b_ALCD == ALCD_water ) 
    b_ALCD[cond] = LIS_land 
    cond = np.where(b_ALCD == ALCD_bckgrnd ) 
    b_ALCD[cond] = LIS_land 
    cond = np.where(b_ALCD == ALCD_land ) 
    b_ALCD[cond] = LIS_land 
    cond = np.where(b_ALCD == ALCD_lcloud) 
    b_ALCD[cond] = LIS_cloud
    cond = np.where(b_ALCD == ALCD_hcloud) 
    b_ALCD[cond] = LIS_cloud
    cond = np.where(b_ALCD == ALCD_shadow) 
    b_ALCD[cond] = LIS_cloud
    
    
    cond = np.where((b_ALCD != LIS_nodata) & (b_SEB != LIS_nodata)) 
    
    b_ALCD2 = b_ALCD[cond]
    b_SEB2 = b_SEB[cond]
    
    
    #ALCD VS LIS
    f = open(os.path.join(path_comp,'info.txt'), 'w')
    print("snow/land/cloud confusion matrix")
    cm = np.array2string(confusion_matrix(b_ALCD2 , b_SEB2,labels=[100,0,205]))
    
    
    print ("kappas")
    kappa = cohen_kappa_score(b_ALCD2 , b_SEB2,labels=[100,0,205])
    print ("OA")
    OA = accuracy_score(b_ALCD2 , b_SEB2)
    print ("F1 scores")
    F1 = f1_score(b_ALCD2 , b_SEB2, average=None,labels=[100,0,205])
    F1_snow = F1[0]
    F1_land = F1[1]
    F1_cloud = F1[2]
    
    print ("snow/land confusion matrix")
    
    cond = np.where((b_ALCD2 != LIS_cloud) & (b_SEB2 != LIS_cloud)) 
    
    b_ALCD3 = b_ALCD2[cond]
    b_SEB3 = b_SEB2[cond]
    
    cm2 = np.array2string(confusion_matrix(b_ALCD3 , b_SEB3,labels=[100,0]))
    
    if k == 0:
        cm_tot = cm
        cm2_tot = cm2
    else :
        cm_tot.__add__(cm)
        cm2_tot.__add__(cm2)
    print ("kappas")
    kappa2 = cohen_kappa_score(b_ALCD3 , b_SEB3,labels=[100,0])
    print ("OA")
    OA2 = accuracy_score(b_ALCD3 , b_SEB3)
    print ("F1 scores")
    F12 = f1_score(b_ALCD3 , b_SEB3, pos_label=100,average=None,labels=[100,0])
    F12_snow = F12[0]
    F12_land = F12[1]

    
    print ("pixel repartitions")
    cond_LIS = np.where((b_SEB == 205) | (b_SEB == 100) | (b_SEB == 0)) 
    cond_ALCD = np.where((b_ALCD == 205) | (b_ALCD == 100) | (b_ALCD == 0)) 
    tot_LIS = float(len(b_SEB[cond_LIS]))
    tot_ALCD = float(len(b_ALCD[cond_ALCD]))
    px_snow_LIS = 100*float(len(b_SEB[b_SEB == 100]))/tot_LIS
    px_land_LIS = 100*float(len(b_SEB[b_SEB == 0]))/tot_LIS
    px_cloud_LIS = 100*float(len(b_SEB[b_SEB == 205]))/tot_LIS
    px_snow_ALCD = 100*float(len(b_ALCD[b_ALCD == 100]))/tot_ALCD
    px_land_ALCD = 100*float(len(b_ALCD[b_ALCD == 0]))/tot_ALCD
    px_cloud_ALCD = 100*float(len(b_ALCD[b_ALCD == 205]))/tot_ALCD
    
    list_b_ALCD.append(b_ALCD.flatten())
    list_b_SEB.append(b_SEB.flatten())
    f.write('Comparison ALCD-LIS\nConfusion Matrix ALCD vs LIS Snow/Land/Cloud\ny axis : ALCD (true); x axis : LIS (pred) \ncol order: snow (top), land (middle), cloud (bottom)\n{}\n\nKAPPA = {};\nOA = {};\nF1_snow = {};\nF1_land = {};\nF1_cloud = {};\nConfusion Matrix ALCD vs LIS Snow/Land/\ny axis : ALCD (true); x axis : LIS (pred) \ncol order: snow (top), land (bottom)\n{}\n\nKAPPA = {};\nOA = {};\nF1_snow = {};\nF1_land = {};\n\nPixel repartition:\nSnow pixels:\n  LIS: {}\n  ALCD: {}\nLand pixels:\n  LIS: {}\n  ALCD: {}\nCloud pixels:\n  LIS: {}\n  ALCD: {}'.format(cm,kappa,OA,F1_snow,F1_land,F1_cloud,cm2,kappa2,OA2,F12_snow,F12_land,px_snow_LIS,px_snow_ALCD,px_land_LIS,px_land_ALCD,px_cloud_LIS,px_cloud_ALCD))
    

    # ALCD SAMPLES METRICS
    matrix_path = os.path.join(list_path_dates_ALCD[k],"Statistics/K_fold_0/confusion_matrix.csv")
    #snow
    a_class = [7]
    b_classes = [3,4,5]
    accuracy, f1, precision, recall, specificity = binary_stats(multi_to_binary_confusion_matrix(a_class,b_classes,matrix_path),all_stats=True)
    
    print(accuracy, f1, precision, recall, specificity)
    f.write('\n\n MATRIX METRICS OF ALCD SAMPLES\nSNOW\n Accuracy\n F1\n Precision\n Recall \n Specificity\n{}\n{}\n{}\n{}\n{}'.format(accuracy, f1, precision, recall, specificity))
    
    #land
    a_class = [5]
    b_classes = [3,4,7]
    accuracy, f1, precision, recall, specificity = binary_stats(multi_to_binary_confusion_matrix(a_class,b_classes,matrix_path),all_stats=True)
    
    print(accuracy, f1, precision, recall, specificity)
    f.write('\nLAND\n Accuracy\n F1\n Precision\n Recall \n Specificity\n{}\n{}\n{}\n{}\n{}'.format(accuracy, f1, precision, recall, specificity))
    
    #cloud
    a_class = [3,4]
    b_classes = [5,7]
    accuracy, f1, precision, recall, specificity = binary_stats(multi_to_binary_confusion_matrix(a_class,b_classes,matrix_path),all_stats=True)
    
    print(accuracy, f1, precision, recall, specificity)
    f.write('\nCLOUD\n Accuracy\n F1\n Precision\n Recall \n Specificity\n{}\n{}\n{}\n{}\n{}'.format(accuracy, f1, precision, recall, specificity))



print(cm_tot)
print("\n\n\n")
print(cm2_tot)
    
    
    # b_ALCD_snow = b_ALCD.copy()
    # b_SEB_snow = b_SEB.copy()
    # b_ALCD_snow[ b_ALCD_snow == 205] = 0
    # b_SEB_snow[ b_SEB_snow == 205] = 0
    # kappa_snow = cohen_kappa_score(b_ALCD_snow.flatten() , b_SEB_snow.flatten(),labels=[100,0])
    # b_ALCD_cloud = b_ALCD.copy()
    # b_SEB_cloud = b_SEB.copy()
    # b_ALCD_cloud[ b_ALCD_cloud== 100] = 0
    # b_SEB_cloud[ b_SEB_cloud == 100] = 0
    # list_b_ALCD_snow.append(b_ALCD_snow )
    # list_b_SEB_snow.append(b_SEB_snow )
    # list_b_ALCD_cloud.append(b_ALCD_cloud  )
    # list_b_SEB_cloud.append(b_SEB_cloud  )
    # kappa_cloud = cohen_kappa_score(b_ALCD_cloud.flatten() , b_SEB_cloud.flatten(),labels=[0,205])
    # f.write('\n\nKappa between snow and cloud/land = {}\n\nKappa between cloud and snow/land = {}'.format(kappa_snow,kappa_cloud ))
    # f.write('\n\ncomp.tif values:\n    snow (LIS) -> land (ALCD) = 6 \n    land (LIS) -> snow (ALCD) = 5 \n    snow (LIS) -> cloud (ALCD) = 4 \n    cloud (LIS) -> snow (ALCD) = 3 \n    cloud (LIS) -> land (ALCD) = 2 \n    land (LIS) -> cloud (ALCD) = 1')
    
    # f.close()
    
    

    # #comparison map LIS-class
    # snow_land = 6
    # land_snow = 5
    # snow_cloud = 4
    # cloud_snow = 3
    # cloud_land = 2
    # land_cloud = 1
    
   
    # print("generation of comparison image")
    # cond = np.where(b_ALCD == b_SEB) 
    # b_comp[cond] = 0
    # cond = np.where((b_SEB == 100) & (b_ALCD == 0))  
    # b_comp[cond] = snow_land
    # cond = np.where((b_SEB == 0) & (b_ALCD == 100 ))  
    # b_comp[cond] = land_snow
    # cond = np.where((b_SEB == 100) & (b_ALCD == 205))  
    # b_comp[cond] = snow_cloud
    # cond = np.where((b_SEB == 205) & (b_ALCD == 100))  
    # b_comp[cond] = cloud_snow
    # cond = np.where((b_SEB == 205) & (b_ALCD == 0 ))  
    # b_comp[cond] = cloud_land
    # cond = np.where((b_SEB == 0) & (b_ALCD == 205))  
    # b_comp[cond] = land_cloud
    
    
    # g_comp.GetRasterBand(1).WriteArray(b_comp)
    # gdal.Translate(os.path.join(path_comp,"comp.tif"),g_comp,format= 'GTiff',noData = 0)



    # #histograms
    
    # print("generation of histograms") # attention aux ndsi <= 0
    # cond1 = np.where((b_comp == land_snow)  ) 
    # cond2 = np.where( (b_comp == cloud_snow) ) 
    # a_swir1 = b_swir[cond1].flatten().astype(float)
    # a_red1 = b_red[cond1].flatten().astype(float)
    # a_green1 = b_green[cond1].flatten().astype(float)
    # a_ndsi1 = (a_green1 - a_swir1) / (a_green1 + a_swir1)
    # a_ndsi1 = a_ndsi1[np.where((~np.isnan(a_ndsi1)) & (~np.isinf(a_ndsi1))) ]
    # a_swir2 = b_swir[cond2].flatten().astype(float)
    # a_red2 = b_red[cond2].flatten().astype(float)
    # a_green2 = b_green[cond2].flatten().astype(float)
    # a_ndsi2 = (a_green2 - a_swir2) / (a_green2 + a_swir2)
    # a_ndsi2 = a_ndsi2[np.where((~np.isnan(a_ndsi2)) & (~np.isinf(a_ndsi2))) ]
    # print(len(a_ndsi1),len(a_ndsi2))
    
    # # Plot figure with subplots 
    # fig = plt.figure()
    # st = fig.suptitle("histogram for LIS missing snow",size = 16)
    # gridspec.GridSpec(2,2)
    
    
    
    # # 1D histo de ir
    # ax = plt.subplot2grid((2,2), (0,0),rowspan=1, colspan=2)
    # plt.title("SWIR band histogram",size = 14,y=1.08)
    # plt.ylabel('percent of pixels',size = 14)
    # plt.xlabel('SWIR band values',size = 14)
    # plt.tick_params(axis = 'both', which = 'major', labelsize = 12)
    # #xticks = np.arange(-1.0, 1.1, 0.1)
    # #plt.xticks(xticks)
    # plt.hist(a_swir1,bins=40,weights=np.ones(len(a_swir1)) / len(a_swir1),alpha=0.5, label='land')
    # plt.hist(a_swir2,bins=40,weights=np.ones(len(a_swir2)) / len(a_swir2),alpha=0.5, label='cloud')
    # plt.gca().yaxis.set_major_formatter(PercentFormatter(1))
    # plt.grid(True) 
    # plt.legend(loc='upper right')
    # # 1D histo de ndsi
    
    
    # ax = plt.subplot2grid((2,2), (1,0),rowspan=1, colspan=2)
    # plt.title("NDSI band histogram",size = 14,y=1.08)
    # plt.ylabel('percent of pixels',size = 14)
    # plt.xlabel('NDSI band values',size = 14)
    # plt.tick_params(axis = 'both', which = 'major', labelsize = 12)
    # #xticks = np.arange(-1.0, 1.1, 0.1)
    # #plt.xticks(xticks)
    # plt.hist(a_ndsi1,bins=40,weights=np.ones(len(a_ndsi1)) / len(a_ndsi1),alpha=0.5, label='land')
    # plt.hist(a_ndsi2,bins=40,weights=np.ones(len(a_ndsi2)) / len(a_ndsi2),alpha=0.5, label='cloud')
    # plt.gca().yaxis.set_major_formatter(PercentFormatter(1))
    # plt.grid(True) 
    # plt.xlim(-3.0,3.0)
    # plt.legend(loc='upper right')
    
    # # fit subplots and save fig
    # fig.tight_layout()
    # fig.set_size_inches(w=16,h=10)
    # st.set_y(0.95)
    # fig.subplots_adjust(top=0.85)
    # fig.savefig(os.path.join(path_comp,"comp.png"))
    # plt.close(fig)
    
    

# if combine == True:
    # print("combinaison of dates data")
    # list_b_ALCD = np.hstack(list_b_ALCD)
    # list_b_SEB = np.hstack(list_b_SEB)
    
    # list_b_ALCD_snow = np.hstack(list_b_ALCD_snow)
    # list_b_SEB_snow = np.hstack( list_b_SEB_snow)
    # list_b_ALCD_cloud = np.hstack( list_b_ALCD_cloud)
    # list_b_SEB_cloud = np.hstack(list_b_SEB_cloud )
    
    # print("confusion matrix for all dates")
    # cm = np.array2string(confusion_matrix(list_b_ALCD,list_b_SEB,labels=[100,0,205]))
    # print("kappas for all dates")
    # kappa = cohen_kappa_score(list_b_ALCD ,list_b_SEB,labels=[100,0,205])
    # kappa_snow = cohen_kappa_score(list_b_ALCD_snow.flatten() , list_b_SEB_snow.flatten(),labels=[100,0])
    # kappa_cloud = cohen_kappa_score(list_b_ALCD_cloud.flatten() , list_b_SEB_cloud.flatten(),labels=[0,205])
    # print("writing results in file")
    # f = open(os.path.join(path_data_ALCD,'info.txt'), 'w')
    # f.write('Comparison ALCD-LIS\nConfusion Matrix ALCD vs LIS\ny axis : ALCD; x axis : LIS\ncol order: snow, land, cloud\n{}\n\nKAPPA = {}'.format(cm,kappa))
    # f.write('\n\nKappa between snow and cloud/land = {}\n\nKappa between cloud and snow/land = {}'.format(kappa_snow,kappa_cloud ))
    # f.write('\n\ncomp.tif values:\n    snow (LIS) -> land (ALCD) = 6 \n    land (LIS) -> snow (ALCD) = 5 \n    snow (LIS) -> cloud (ALCD) = 4 \n    cloud (LIS) -> snow (ALCD) = 3 \n    cloud (LIS) -> land (ALCD) = 2 \n    land (LIS) -> cloud (ALCD) = 1')
    # f.close()
	
