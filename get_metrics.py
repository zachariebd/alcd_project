
import sys
import os
import errno
import re
import math
import json
import numpy as np
import argparse
import matplotlib
#matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.colors import LogNorm
from matplotlib.ticker import PercentFormatter
from scipy.stats import mstats
import pandas as pd
import seaborn as sns


tile = "T31TCH"

original_red_pass1=200
original_red_pass2=40
original_ndsi_pass1=0.4
original_ndsi_pass2=0.15
original_params = [original_ndsi_pass1,original_ndsi_pass2,original_red_pass1,original_red_pass2]
string_original_params = " ".join(str(i) for i in original_params)

dict_comb= {}
n = 0
with open('results_days.txt', 'r') as f:
    next(f)

    for line in f:
        n = n+1
        print(n)
        split_line = line.split()
        L2A = split_line[0]
        print(L2A)
        date = split_line[1]
        print(date)
        params = split_line[2:6]
        print(params)
        string_params = "".join(i for i in params)
        
        n1 = float(params[0])
        n2 = float(params[1])
        r1 = float(params[2])
        r2 = float(params[3])
        params = [n1,n2,r1,r2]
        cm = split_line[7:]
        if string_params not in dict_comb:
            dict_comb[string_params] = [params,cm]
        else :
            dict_comb[string_params][1] = [int(a) + int(b) for a, b in zip(dict_comb[string_params][1] , cm )]
        
        
        
kappa_out = open('results_period.txt', 'w')
kappa_out.write("ndsi_pass1 ndsi_pass2 red_pass1 red_pass2 kappa cm11 cm12 cm13 cm21 cm22 cm23 cm31 cm32 cm33")

#print(dict_comb)

#print("\n\n")

list_n1 =[]
list_n2 = []
list_r1 = []
list_r2 = []
list_kappa = []
list_OA = []
list_F1_snow = []
list_F1_land = []
list_F1_cloud = []
original_kappa = 0
original_OA = 0
original_F1_snow = 0
original_F1_land = 0
original_F1_cloud = 0

best_metrics = {}
best_metrics["best_kappa"]={}
best_metrics["best_kappa"]["kappa"]=0
best_metrics["best_kappa"]["macro_precision"]=0
best_metrics["best_kappa"]["micro_precision"]=0
best_metrics["best_kappa"]["macro_recall"]=0
best_metrics["best_kappa"]["micro_recall"]=0
best_metrics["best_kappa"]["macro_F1"]=0
best_metrics["best_kappa"]["micro_F1"]=0
best_metrics["best_kappa"]["params"]=[0,0,0,0]
best_metrics["best_kappa"]["snow_F1"]=0
best_metrics["best_kappa"]["land_F1"]=0
best_metrics["best_kappa"]["cloud_F1"]=0
best_metrics["best_kappa"]["overall_accuracy"]=0

best_metrics["best_macro_precision"]={}
best_metrics["best_macro_precision"]["kappa"]=0
best_metrics["best_macro_precision"]["macro_precision"]=0
best_metrics["best_macro_precision"]["micro_precision"]=0
best_metrics["best_macro_precision"]["macro_recall"]=0
best_metrics["best_macro_precision"]["micro_recall"]=0
best_metrics["best_macro_precision"]["macro_F1"]=0
best_metrics["best_macro_precision"]["micro_F1"]=0
best_metrics["best_macro_precision"]["params"]=[0,0,0,0]
best_metrics["best_macro_precision"]["snow_F1"]=0
best_metrics["best_macro_precision"]["land_F1"]=0
best_metrics["best_macro_precision"]["cloud_F1"]=0
best_metrics["best_macro_precision"]["overall_accuracy"]=0

best_metrics["best_micro_precision"]={}
best_metrics["best_micro_precision"]["kappa"]=0
best_metrics["best_micro_precision"]["macro_precision"]=0
best_metrics["best_micro_precision"]["micro_precision"]=0
best_metrics["best_micro_precision"]["macro_recall"]=0
best_metrics["best_micro_precision"]["micro_recall"]=0
best_metrics["best_micro_precision"]["macro_F1"]=0
best_metrics["best_micro_precision"]["micro_F1"]=0
best_metrics["best_micro_precision"]["params"]=[0,0,0,0]
best_metrics["best_micro_precision"]["snow_F1"]=0
best_metrics["best_micro_precision"]["land_F1"]=0
best_metrics["best_micro_precision"]["cloud_F1"]=0
best_metrics["best_micro_precision"]["overall_accuracy"]=0

best_metrics["best_macro_recall"]={}
best_metrics["best_macro_recall"]["kappa"]=0
best_metrics["best_macro_recall"]["macro_precision"]=0
best_metrics["best_macro_recall"]["micro_precision"]=0
best_metrics["best_macro_recall"]["macro_recall"]=0
best_metrics["best_macro_recall"]["micro_recall"]=0
best_metrics["best_macro_recall"]["macro_F1"]=0
best_metrics["best_macro_recall"]["micro_F1"]=0
best_metrics["best_macro_recall"]["params"]=[0,0,0,0]
best_metrics["best_macro_recall"]["snow_F1"]=0
best_metrics["best_macro_recall"]["land_F1"]=0
best_metrics["best_macro_recall"]["cloud_F1"]=0
best_metrics["best_macro_recall"]["overall_accuracy"]=0

best_metrics["best_micro_recall"]={}
best_metrics["best_micro_recall"]["kappa"]=0
best_metrics["best_micro_recall"]["macro_precision"]=0
best_metrics["best_micro_recall"]["micro_precision"]=0
best_metrics["best_micro_recall"]["macro_recall"]=0
best_metrics["best_micro_recall"]["micro_recall"]=0
best_metrics["best_micro_recall"]["macro_F1"]=0
best_metrics["best_micro_recall"]["micro_F1"]=0
best_metrics["best_micro_recall"]["params"]=[0,0,0,0]
best_metrics["best_micro_recall"]["snow_F1"]=0
best_metrics["best_micro_recall"]["land_F1"]=0
best_metrics["best_micro_recall"]["cloud_F1"]=0
best_metrics["best_micro_recall"]["overall_accuracy"]=0

best_metrics["best_macro_F1"]={}
best_metrics["best_macro_F1"]["kappa"]=0
best_metrics["best_macro_F1"]["macro_precision"]=0
best_metrics["best_macro_F1"]["micro_precision"]=0
best_metrics["best_macro_F1"]["macro_recall"]=0
best_metrics["best_macro_F1"]["micro_recall"]=0
best_metrics["best_macro_F1"]["macro_F1"]=0
best_metrics["best_macro_F1"]["micro_F1"]=0
best_metrics["best_macro_F1"]["params"]=[0,0,0,0]
best_metrics["best_macro_F1"]["snow_F1"]=0
best_metrics["best_macro_F1"]["land_F1"]=0
best_metrics["best_macro_F1"]["cloud_F1"]=0
best_metrics["best_macro_F1"]["overall_accuracy"]=0

best_metrics["best_micro_F1"]={}
best_metrics["best_micro_F1"]["kappa"]=0
best_metrics["best_micro_F1"]["macro_precision"]=0
best_metrics["best_micro_F1"]["micro_precision"]=0
best_metrics["best_micro_F1"]["macro_recall"]=0
best_metrics["best_micro_F1"]["micro_recall"]=0
best_metrics["best_micro_F1"]["macro_F1"]=0
best_metrics["best_micro_F1"]["micro_F1"]=0
best_metrics["best_micro_F1"]["params"]=[0,0,0,0]
best_metrics["best_micro_F1"]["snow_F1"]=0
best_metrics["best_micro_F1"]["land_F1"]=0
best_metrics["best_micro_F1"]["cloud_F1"]=0
best_metrics["best_micro_F1"]["overall_accuracy"]=0

best_metrics["best_snow_F1"]={}
best_metrics["best_snow_F1"]["kappa"]=0
best_metrics["best_snow_F1"]["macro_precision"]=0
best_metrics["best_snow_F1"]["micro_precision"]=0
best_metrics["best_snow_F1"]["macro_recall"]=0
best_metrics["best_snow_F1"]["micro_recall"]=0
best_metrics["best_snow_F1"]["macro_F1"]=0
best_metrics["best_snow_F1"]["micro_F1"]=0
best_metrics["best_snow_F1"]["params"]=[0,0,0,0]
best_metrics["best_snow_F1"]["snow_F1"]=0
best_metrics["best_snow_F1"]["land_F1"]=0
best_metrics["best_snow_F1"]["cloud_F1"]=0
best_metrics["best_snow_F1"]["overall_accuracy"]=0

best_metrics["best_land_F1"]={}
best_metrics["best_land_F1"]["kappa"]=0
best_metrics["best_land_F1"]["macro_precision"]=0
best_metrics["best_land_F1"]["micro_precision"]=0
best_metrics["best_land_F1"]["macro_recall"]=0
best_metrics["best_land_F1"]["micro_recall"]=0
best_metrics["best_land_F1"]["macro_F1"]=0
best_metrics["best_land_F1"]["micro_F1"]=0
best_metrics["best_land_F1"]["params"]=[0,0,0,0]
best_metrics["best_land_F1"]["snow_F1"]=0
best_metrics["best_land_F1"]["land_F1"]=0
best_metrics["best_land_F1"]["cloud_F1"]=0
best_metrics["best_land_F1"]["overall_accuracy"]=0

best_metrics["best_cloud_F1"]={}
best_metrics["best_cloud_F1"]["kappa"]=0
best_metrics["best_cloud_F1"]["macro_precision"]=0
best_metrics["best_cloud_F1"]["micro_precision"]=0
best_metrics["best_cloud_F1"]["macro_recall"]=0
best_metrics["best_cloud_F1"]["micro_recall"]=0
best_metrics["best_cloud_F1"]["macro_F1"]=0
best_metrics["best_cloud_F1"]["micro_F1"]=0
best_metrics["best_cloud_F1"]["params"]=[0,0,0,0]
best_metrics["best_cloud_F1"]["snow_F1"]=0
best_metrics["best_cloud_F1"]["land_F1"]=0
best_metrics["best_cloud_F1"]["cloud_F1"]=0
best_metrics["best_cloud_F1"]["overall_accuracy"]=0

best_metrics["best_overall_accuracy"]={}
best_metrics["best_overall_accuracy"]["kappa"]=0
best_metrics["best_overall_accuracy"]["macro_precision"]=0
best_metrics["best_overall_accuracy"]["micro_precision"]=0
best_metrics["best_overall_accuracy"]["macro_recall"]=0
best_metrics["best_overall_accuracy"]["micro_recall"]=0
best_metrics["best_overall_accuracy"]["macro_F1"]=0
best_metrics["best_overall_accuracy"]["micro_F1"]=0
best_metrics["best_overall_accuracy"]["params"]=[0,0,0,0]
best_metrics["best_overall_accuracy"]["snow_F1"]=0
best_metrics["best_overall_accuracy"]["land_F1"]=0
best_metrics["best_overall_accuracy"]["cloud_F1"]=0
best_metrics["best_overall_accuracy"]["overall_accuracy"]=0


for string_params in dict_comb.keys() :
    cm = dict_comb[string_params][1]
    params = dict_comb[string_params][0]
    #1: snow, 2: land, 3: cloud
    #ALCD(true) - LIS(pred)
    #23 = land ALCD - cloud LIS
    cm11 = cm[0] 
    cm12 = cm[1]
    cm13 = cm[2]
    cm21 = cm[3]
    cm22 = cm[4]
    cm23 = cm[5]
    cm31 = cm[6]
    cm32 = cm[7]
    cm33 = cm[8]
    n1 = params[0]
    n2 = params[1]
    r1 = params[2]
    r2 = params[3]
    

    #calculation of snow/no snow metrics
    TP_snow = float(cm11)
    TN_snow = float(cm22 + cm33 + cm23 + cm32)
    FP_snow = float(cm21 + cm31)
    FN_snow = float(cm12 + cm13)
    Precision_snow = TP_snow/(TP_snow + FP_snow)
    Recall_snow = TP_snow/(TP_snow+FN_snow)
    F1_snow = 2*Precision_snow*Recall_snow/(Precision_snow+Recall_snow)

    
    #calculation of land/no land metrics
    TP_land = float(cm22)
    TN_land = float(cm11 + cm33 + cm13 + cm31)
    FP_land = float(cm12 + cm32)
    FN_land = float(cm21 + cm23)
    Precision_land = TP_land/(TP_land + FP_land)
    Recall_land= TP_land/(TP_land+FN_land)
    F1_land= 2*Precision_land*Recall_land/(Precision_land+Recall_land)

    
    #calculation of cloud/no cloud metrics
    TP_cloud = float(cm33)
    TN_cloud = float(cm11 + cm22 + cm12 + cm21)
    FP_cloud = float(cm13 + cm23)
    FN_cloud = float(cm31 + cm32)
    Precision_cloud= TP_cloud/(TP_cloud + FP_cloud)
    Recall_cloud= TP_cloud/(TP_cloud+FN_cloud)
    F1_cloud= 2*Precision_cloud*Recall_cloud/(Precision_cloud+Recall_cloud)
    

    #calculation of multiclass metrics
    total = float(cm11+cm12+cm13+cm21+cm22+cm23+cm31+cm32+cm33)
    Agree = float(cm11+cm22+cm33)/total
    ChanceAgree1 =float(cm11+cm12+cm13)/total * float(cm11+cm21+cm31)/total
    ChanceAgree2 =float(cm21+cm22+cm23)/total * float(cm12+cm22+cm32)/total
    ChanceAgree3 =float(cm31+cm32+cm33)/total * float(cm13+cm23+cm33)/total
    ChanceAgree = ChanceAgree1 + ChanceAgree2 + ChanceAgree3 
    kappa = (Agree - ChanceAgree)/(1 - ChanceAgree)
    micro_precision = (TP_snow+TP_land+TP_cloud)/(TP_snow+TP_land+TP_cloud+FP_snow+FP_land+FP_cloud)
    micro_recall = (TP_snow+TP_land+TP_cloud)/(TP_snow+TP_land+TP_cloud+FN_snow+FN_land+FN_cloud)
    micro_F1 = 2*micro_precision*micro_recall/(micro_precision+micro_recall)
    macro_precision = (Precision_snow+Precision_land+Precision_cloud)/3.0
    macro_recall = (Recall_snow+Recall_land+Recall_cloud)/3.0
    macro_F1 = (F1_snow+F1_land+F1_cloud)/3.0
    
    if best_metrics["best_kappa"]["kappa"] < kappa: 
        best_metrics["best_kappa"]["kappa"]=kappa
        best_metrics["best_kappa"]["macro_precision"]=macro_precision
        best_metrics["best_kappa"]["micro_precision"]=micro_precision
        best_metrics["best_kappa"]["macro_recall"]=macro_recall 
        best_metrics["best_kappa"]["micro_recall"]=micro_recall
        best_metrics["best_kappa"]["macro_F1"]=macro_F1
        best_metrics["best_kappa"]["micro_F1"]=micro_F1
        best_metrics["best_kappa"]["params"]=[n1,n2,r1,r2]
        best_metrics["best_kappa"]["snow_F1"]=F1_snow
        best_metrics["best_kappa"]["land_F1"]=F1_land
        best_metrics["best_kappa"]["cloud_F1"]=F1_cloud
        best_metrics["best_kappa"]["overall_accuracy"]=Agree
        

    if best_metrics["best_macro_precision"]["macro_precision"] < macro_precision: 
        best_metrics["best_macro_precision"]["kappa"]=kappa
        best_metrics["best_macro_precision"]["macro_precision"]=macro_precision
        best_metrics["best_macro_precision"]["micro_precision"]=micro_precision
        best_metrics["best_macro_precision"]["macro_recall"]=macro_recall 
        best_metrics["best_macro_precision"]["micro_recall"]=micro_recall
        best_metrics["best_macro_precision"]["macro_F1"]=macro_F1
        best_metrics["best_macro_precision"]["micro_F1"]=micro_F1
        best_metrics["best_macro_precision"]["params"]=[n1,n2,r1,r2]   
        best_metrics["best_macro_precision"]["snow_F1"]=F1_snow
        best_metrics["best_macro_precision"]["land_F1"]=F1_land
        best_metrics["best_macro_precision"]["cloud_F1"]=F1_cloud
        best_metrics["best_macro_precision"]["overall_accuracy"]=Agree
    
    if best_metrics["best_micro_precision"]["micro_precision"] < micro_precision: 
        best_metrics["best_micro_precision"]["kappa"]=kappa
        best_metrics["best_micro_precision"]["macro_precision"]=macro_precision
        best_metrics["best_micro_precision"]["micro_precision"]=micro_precision
        best_metrics["best_micro_precision"]["macro_recall"]=macro_recall 
        best_metrics["best_micro_precision"]["micro_recall"]=micro_recall
        best_metrics["best_micro_precision"]["macro_F1"]=macro_F1
        best_metrics["best_micro_precision"]["micro_F1"]=micro_F1
        best_metrics["best_micro_precision"]["params"]=[n1,n2,r1,r2]      
        best_metrics["best_micro_precision"]["snow_F1"]=F1_snow
        best_metrics["best_micro_precision"]["land_F1"]=F1_land
        best_metrics["best_micro_precision"]["cloud_F1"]=F1_cloud
        best_metrics["best_micro_precision"]["overall_accuracy"]=Agree
    
    if best_metrics["best_macro_recall"]["macro_recall"] < macro_recall: 
        best_metrics["best_macro_recall"]["kappa"]=kappa
        best_metrics["best_macro_recall"]["macro_precision"]=macro_precision
        best_metrics["best_macro_recall"]["micro_precision"]=micro_precision
        best_metrics["best_macro_recall"]["macro_recall"]=macro_recall 
        best_metrics["best_macro_recall"]["micro_recall"]=micro_recall
        best_metrics["best_macro_recall"]["macro_F1"]=macro_F1
        best_metrics["best_macro_recall"]["micro_F1"]=micro_F1
        best_metrics["best_macro_recall"]["params"]=[n1,n2,r1,r2]   
        best_metrics["best_macro_recall"]["snow_F1"]=F1_snow
        best_metrics["best_macro_recall"]["land_F1"]=F1_land
        best_metrics["best_macro_recall"]["cloud_F1"]=F1_cloud
        best_metrics["best_macro_recall"]["overall_accuracy"]=Agree
    
    if best_metrics["best_micro_recall"]["micro_recall"] < micro_recall: 
        best_metrics["best_micro_recall"]["kappa"]=kappa
        best_metrics["best_micro_recall"]["macro_precision"]=macro_precision
        best_metrics["best_micro_recall"]["micro_precision"]=micro_precision
        best_metrics["best_micro_recall"]["macro_recall"]=macro_recall 
        best_metrics["best_micro_recall"]["micro_recall"]=micro_recall
        best_metrics["best_micro_recall"]["macro_F1"]=macro_F1
        best_metrics["best_micro_recall"]["micro_F1"]=micro_F1
        best_metrics["best_micro_recall"]["params"]=[n1,n2,r1,r2]      
        best_metrics["best_micro_recall"]["snow_F1"]=F1_snow
        best_metrics["best_micro_recall"]["land_F1"]=F1_land
        best_metrics["best_micro_recall"]["cloud_F1"]=F1_cloud
        best_metrics["best_micro_recall"]["overall_accuracy"]=Agree   
    
    
    if best_metrics["best_macro_F1"]["macro_F1"] < macro_F1: 
        best_metrics["best_macro_F1"]["kappa"]=kappa
        best_metrics["best_macro_F1"]["macro_precision"]=macro_precision
        best_metrics["best_macro_F1"]["micro_precision"]=micro_precision
        best_metrics["best_macro_F1"]["macro_recall"]=macro_recall 
        best_metrics["best_macro_F1"]["micro_recall"]=micro_recall
        best_metrics["best_macro_F1"]["macro_F1"]=macro_F1
        best_metrics["best_macro_F1"]["micro_F1"]=micro_F1
        best_metrics["best_macro_F1"]["params"]=[n1,n2,r1,r2]   
        best_metrics["best_macro_F1"]["snow_F1"]=F1_snow
        best_metrics["best_macro_F1"]["land_F1"]=F1_land
        best_metrics["best_macro_F1"]["cloud_F1"]=F1_cloud
        best_metrics["best_macro_F1"]["overall_accuracy"]=Agree
    
    if best_metrics["best_micro_F1"]["micro_F1"] < micro_F1: 
        best_metrics["best_micro_F1"]["kappa"]=kappa
        best_metrics["best_micro_F1"]["macro_precision"]=macro_precision
        best_metrics["best_micro_F1"]["micro_precision"]=micro_precision
        best_metrics["best_micro_F1"]["macro_recall"]=macro_recall 
        best_metrics["best_micro_F1"]["micro_recall"]=micro_recall
        best_metrics["best_micro_F1"]["macro_F1"]=macro_F1
        best_metrics["best_micro_F1"]["micro_F1"]=micro_F1
        best_metrics["best_micro_F1"]["params"]=[n1,n2,r1,r2]    
        best_metrics["best_micro_F1"]["snow_F1"]=F1_snow
        best_metrics["best_micro_F1"]["land_F1"]=F1_land
        best_metrics["best_micro_F1"]["cloud_F1"]=F1_cloud
        best_metrics["best_micro_F1"]["overall_accuracy"]=Agree


    if best_metrics["best_snow_F1"]["snow_F1"] < F1_snow:
        best_metrics["best_snow_F1"]["kappa"]=kappa
        best_metrics["best_snow_F1"]["macro_precision"]=macro_precision
        best_metrics["best_snow_F1"]["micro_precision"]=micro_precision
        best_metrics["best_snow_F1"]["macro_recall"]=macro_recall 
        best_metrics["best_snow_F1"]["micro_recall"]=micro_recall
        best_metrics["best_snow_F1"]["macro_F1"]=macro_F1
        best_metrics["best_snow_F1"]["micro_F1"]=micro_F1
        best_metrics["best_snow_F1"]["params"]=[n1,n2,r1,r2]    
        best_metrics["best_snow_F1"]["snow_F1"]=F1_snow
        best_metrics["best_snow_F1"]["land_F1"]=F1_land
        best_metrics["best_snow_F1"]["cloud_F1"]=F1_cloud
        best_metrics["best_snow_F1"]["overall_accuracy"]=Agree
        
    if best_metrics["best_land_F1"]["land_F1"] < F1_land:
        best_metrics["best_land_F1"]["kappa"]=kappa
        best_metrics["best_land_F1"]["macro_precision"]=macro_precision
        best_metrics["best_land_F1"]["micro_precision"]=micro_precision
        best_metrics["best_land_F1"]["macro_recall"]=macro_recall 
        best_metrics["best_land_F1"]["micro_recall"]=micro_recall
        best_metrics["best_land_F1"]["macro_F1"]=macro_F1
        best_metrics["best_land_F1"]["micro_F1"]=micro_F1
        best_metrics["best_land_F1"]["params"]=[n1,n2,r1,r2]    
        best_metrics["best_land_F1"]["snow_F1"]=F1_snow
        best_metrics["best_land_F1"]["land_F1"]=F1_land
        best_metrics["best_land_F1"]["cloud_F1"]=F1_cloud
        best_metrics["best_land_F1"]["overall_accuracy"]=Agree
        
    if best_metrics["best_cloud_F1"]["cloud_F1"] < F1_cloud:
        best_metrics["best_cloud_F1"]["kappa"]=kappa
        best_metrics["best_cloud_F1"]["macro_precision"]=macro_precision
        best_metrics["best_cloud_F1"]["micro_precision"]=micro_precision
        best_metrics["best_cloud_F1"]["macro_recall"]=macro_recall 
        best_metrics["best_cloud_F1"]["micro_recall"]=micro_recall
        best_metrics["best_cloud_F1"]["macro_F1"]=macro_F1
        best_metrics["best_cloud_F1"]["micro_F1"]=micro_F1
        best_metrics["best_cloud_F1"]["params"]=[n1,n2,r1,r2]    
        best_metrics["best_cloud_F1"]["snow_F1"]=F1_snow
        best_metrics["best_cloud_F1"]["land_F1"]=F1_land
        best_metrics["best_cloud_F1"]["cloud_F1"]=F1_cloud
        best_metrics["best_cloud_F1"]["overall_accuracy"]=Agree
        
    if best_metrics["best_overall_accuracy"]["overall_accuracy"] < Agree:
        best_metrics["best_overall_accuracy"]["kappa"]=kappa
        best_metrics["best_overall_accuracy"]["macro_precision"]=macro_precision
        best_metrics["best_overall_accuracy"]["micro_precision"]=micro_precision
        best_metrics["best_overall_accuracy"]["macro_recall"]=macro_recall 
        best_metrics["best_overall_accuracy"]["micro_recall"]=micro_recall
        best_metrics["best_overall_accuracy"]["macro_F1"]=macro_F1
        best_metrics["best_overall_accuracy"]["micro_F1"]=micro_F1
        best_metrics["best_overall_accuracy"]["params"]=[n1,n2,r1,r2]    
        best_metrics["best_overall_accuracy"]["snow_F1"]=F1_snow
        best_metrics["best_overall_accuracy"]["land_F1"]=F1_land
        best_metrics["best_overall_accuracy"]["cloud_F1"]=F1_cloud
        best_metrics["best_overall_accuracy"]["overall_accuracy"]=Agree


    print(string_params,kappa)      

    kappa_out.write('\n'+str(n1)+' '+str(n2)+' '+str(r1)+' '+str(r2)+' '+str(kappa)+' '+str(cm11)+' '+str(cm12)+' '+str(cm13)+' '+str(cm21)+' '+str(cm22)+' '+str(cm23)+' '+str(cm31)+' '+str(cm32)+' '+str(cm33))
    
    
    if params == original_params : 
        original_kappa = kappa*100
        original_OA = Agree*100
        original_F1_snow = F1_snow*100
        original_F1_land = F1_land*100
        original_F1_cloud = F1_cloud*100
    list_n1.append(n1)
    list_n2.append(n2)
    list_r1.append(r1)
    list_r2.append(r2)
    list_kappa.append(kappa*100)
    list_OA.append(Agree*100)
    list_F1_snow.append(F1_snow*100)
    list_F1_land.append(F1_land*100)
    list_F1_cloud.append(F1_cloud*100)

    

# write metric json
with open('best_metrics.json', 'w',encoding='utf-8') as metric_out:
    json.dump(best_metrics,metric_out, ensure_ascii=False, indent=4)



#################SCATTER PLOTS##################################"
#PLOT KAPPA
# Plot figure with subplots 
fig = plt.figure()
st = fig.suptitle("LIS/ALCD KAPPA")
gridspec.GridSpec(2,2)
ax = plt.subplot2grid((2,2), (0,0))
plt.title('Effect of NDSI pass 1 on kappa')
plt.xlabel('NDSI pass 1')
plt.ylabel('KAPPA')
plt.scatter(list_n1, list_kappa, c = "green")
plt.plot(original_ndsi_pass1,original_kappa,"or")
ax = plt.subplot2grid((2,2), (0,1))
plt.title('Effect of NDSI pass 2 on kappa')
plt.xlabel('NDSI pass 2')
plt.ylabel('KAPPA')
plt.scatter(list_n2, list_kappa, c = "green")
plt.plot(original_ndsi_pass2,original_kappa,"or")
ax = plt.subplot2grid((2,2), (1,0))
plt.title('Effect of red pass 1 on kappa')
plt.xlabel('Red pass 1')
plt.ylabel('KAPPA')
plt.scatter(list_r1, list_kappa, c = "green")
plt.plot(original_red_pass1,original_kappa,"or")
ax = plt.subplot2grid((2,2), (1,1))
plt.title('Effect of red pass 2 on kappa')
plt.xlabel('Red pass 2')
plt.ylabel('KAPPA')
plt.scatter(list_r2, list_kappa, c = "green")
plt.plot(original_red_pass2,original_kappa,"or")
fig.set_size_inches(w=16,h=10)
st.set_y(0.95)
fig.subplots_adjust(top=0.85,wspace=0.2, hspace=0.4)
fig.savefig('kappas_'+tile+'.png')
plt.close(fig)


#PLOT OA
# Plot figure with subplots 
fig = plt.figure()
st = fig.suptitle("LIS/ALCD Overall Accuracy")
gridspec.GridSpec(2,2)
ax = plt.subplot2grid((2,2), (0,0))
plt.title('Effect of NDSI pass 1 on OA')
plt.xlabel('NDSI pass 1')
plt.ylabel('OA')
plt.scatter(list_n1, list_OA, c = "green")
plt.plot(original_ndsi_pass1,original_OA,"or")
ax = plt.subplot2grid((2,2), (0,1))
plt.title('Effect of NDSI pass 2 on OA')
plt.xlabel('NDSI pass 2')
plt.ylabel('OA')
plt.scatter(list_n2, list_OA, c = "green")
plt.plot(original_ndsi_pass2,original_OA,"or")
ax = plt.subplot2grid((2,2), (1,0))
plt.title('Effect of red pass 1 on OA')
plt.xlabel('Red pass 1')
plt.ylabel('OA')
plt.scatter(list_r1, list_OA, c = "green")
plt.plot(original_red_pass1,original_OA,"or")
ax = plt.subplot2grid((2,2), (1,1))
plt.title('Effect of red pass 2 on OA')
plt.xlabel('Red pass 2')
plt.ylabel('OA')
plt.scatter(list_r2, list_OA, c = "green")
plt.plot(original_red_pass2,original_OA,"or")
fig.set_size_inches(w=16,h=10)
st.set_y(0.95)
fig.subplots_adjust(top=0.85,wspace=0.2, hspace=0.4)
fig.savefig('OAs_'+tile+'.png')
plt.close(fig)

#PLOT SNOW F1
# Plot figure with subplots 
fig = plt.figure()
st = fig.suptitle("LIS/ALCD F1_snow")
gridspec.GridSpec(2,2)
ax = plt.subplot2grid((2,2), (0,0))
plt.title('Effect of NDSI pass 1 on F1_snow')
plt.xlabel('NDSI pass 1')
plt.ylabel('F1_snow')
plt.scatter(list_n1, list_F1_snow, c = "green")
plt.plot(original_ndsi_pass1,original_F1_snow,"or")
ax = plt.subplot2grid((2,2), (0,1))
plt.title('Effect of NDSI pass 2 on F1_snow')
plt.xlabel('NDSI pass 2')
plt.ylabel('F1_snow')
plt.scatter(list_n2, list_F1_snow, c = "green")
plt.plot(original_ndsi_pass2,original_F1_snow,"or")
ax = plt.subplot2grid((2,2), (1,0))
plt.title('Effect of red pass 1 on F1_snow')
plt.xlabel('Red pass 1')
plt.ylabel('F1_snow')
plt.scatter(list_r1, list_F1_snow, c = "green")
plt.plot(original_red_pass1,original_F1_snow,"or")
ax = plt.subplot2grid((2,2), (1,1))
plt.title('Effect of red pass 2 on F1_snow')
plt.xlabel('Red pass 2')
plt.ylabel('F1_snow')
plt.scatter(list_r2, list_F1_snow, c = "green")
plt.plot(original_red_pass2,original_F1_snow,"or")
fig.set_size_inches(w=16,h=10)
st.set_y(0.95)
fig.subplots_adjust(top=0.85,wspace=0.2, hspace=0.4)
fig.savefig('F1_snow_'+tile+'.png')
plt.close(fig)

#PLOT LAND F1
# Plot figure with subplots 
fig = plt.figure()
st = fig.suptitle("LIS/ALCD F1_land")
gridspec.GridSpec(2,2)
ax = plt.subplot2grid((2,2), (0,0))
plt.title('Effect of NDSI pass 1 on F1_land')
plt.xlabel('NDSI pass 1')
plt.ylabel('F1_land')
plt.scatter(list_n1, list_F1_land, c = "green")
plt.plot(original_ndsi_pass1,original_F1_land,"or")
ax = plt.subplot2grid((2,2), (0,1))
plt.title('Effect of NDSI pass 2 on F1_land')
plt.xlabel('NDSI pass 2')
plt.ylabel('F1_land')
plt.scatter(list_n2, list_F1_land, c = "green")
plt.plot(original_ndsi_pass2,original_F1_land,"or")
ax = plt.subplot2grid((2,2), (1,0))
plt.title('Effect of red pass 1 on F1_land')
plt.xlabel('Red pass 1')
plt.ylabel('F1_land')
plt.scatter(list_r1, list_F1_land, c = "green")
plt.plot(original_red_pass1,original_F1_land,"or")
ax = plt.subplot2grid((2,2), (1,1))
plt.title('Effect of red pass 2 on F1_land')
plt.xlabel('Red pass 2')
plt.ylabel('F1_land')
plt.scatter(list_r2, list_F1_land, c = "green")
plt.plot(original_red_pass2,original_F1_land,"or")
fig.set_size_inches(w=16,h=10)
st.set_y(0.95)
fig.subplots_adjust(top=0.85,wspace=0.2, hspace=0.4)
fig.savefig('F1_land_'+tile+'.png')
plt.close(fig)

#PLOT CLOUD F1
# Plot figure with subplots 
fig = plt.figure()
st = fig.suptitle("LIS/ALCD F1_cloud")
gridspec.GridSpec(2,2)
ax = plt.subplot2grid((2,2), (0,0))
plt.title('Effect of NDSI pass 1 on F1_cloud')
plt.xlabel('NDSI pass 1')
plt.ylabel('F1_cloud')
plt.scatter(list_n1, list_F1_cloud, c = "green")
plt.plot(original_ndsi_pass1,original_F1_cloud,"or")
ax = plt.subplot2grid((2,2), (0,1))
plt.title('Effect of NDSI pass 2 on F1_cloud')
plt.xlabel('NDSI pass 2')
plt.ylabel('F1_cloud')
plt.scatter(list_n2, list_F1_cloud, c = "green")
plt.plot(original_ndsi_pass2,original_F1_cloud,"or")
ax = plt.subplot2grid((2,2), (1,0))
plt.title('Effect of red pass 1 on F1_cloud')
plt.xlabel('Red pass 1')
plt.ylabel('F1_cloud')
plt.scatter(list_r1, list_F1_cloud, c = "green")
plt.plot(original_red_pass1,original_F1_cloud,"or")
ax = plt.subplot2grid((2,2), (1,1))
plt.title('Effect of red pass 2 on F1_cloud')
plt.xlabel('Red pass 2')
plt.ylabel('F1_cloud')
plt.scatter(list_r2, list_F1_cloud, c = "green")
plt.plot(original_red_pass2,original_F1_cloud,"or")
fig.set_size_inches(w=16,h=10)
st.set_y(0.95)
fig.subplots_adjust(top=0.85,wspace=0.2, hspace=0.4)
fig.savefig('F1_cloud_'+tile+'.png')
plt.close(fig)


###############BOXPLOTS############################

label_size = 20
tick_size = 16
df = pd.DataFrame({'kappa':list_kappa,'OA':list_OA,'F1_snow':list_F1_snow,'F1_land':list_F1_land,'F1_cloud':list_F1_cloud,'n1':list_n1,'n2':list_n2,'r1':list_r1,'r2':list_r2})





#PLOT KAPPA
fig = plt.figure()
#st = fig.suptitle("EFFECTS OF THRESHOLD PARAMETERS ON KAPPA SCORES",size = 18)
gridspec.GridSpec(2,2)
#NDSI 1
ax = plt.subplot2grid((2,2), (0,0))
#plt.title('Effect of NDSI pass 1 on Kappa score',size = 16)
sns.boxplot(x='n1',y='kappa',data=df, color="skyblue")
sns.swarmplot(x='n1',y=df.loc[(df['n1'] == original_ndsi_pass1) & (df['n2'] == original_ndsi_pass2) & (df['r1'] == original_red_pass1) & (df['r2'] == original_red_pass2),'kappa'],data=df, color="red",size=7)
plt.xlabel('NDSI Pass 1',size = label_size)
plt.ylabel('Kappa (%)',size = label_size)
plt.xticks(rotation=45, horizontalalignment='center',size = tick_size)
plt.yticks(size = tick_size)
ax.yaxis.labelpad = 7
#NDSI 2
ax = plt.subplot2grid((2,2), (0,1))
#plt.title('Effect of NDSI pass 2 on Kappa score',size = 16)
sns.boxplot(x='n2',y='kappa',data=df, color="skyblue")
sns.swarmplot(x='n2',y=df.loc[(df['n1'] == original_ndsi_pass1) & (df['n2'] == original_ndsi_pass2) & (df['r1'] == original_red_pass1) & (df['r2'] == original_red_pass2),'kappa'],data=df, color="red",size=7)
plt.xlabel('NDSI Pass 2',size = label_size)
plt.ylabel('Kappa (%)',size = label_size)
plt.xticks(rotation=45, horizontalalignment='center',size = tick_size)
plt.yticks(size = tick_size)
ax.yaxis.labelpad = 7
#RED 1
ax = plt.subplot2grid((2,2), (1,0))
#plt.title('Effect of Red band pass 1 on Kappa score',size = 16)
sns.boxplot(x='r1',y='kappa',data=df, color="skyblue")
sns.swarmplot(x='r1',y=df.loc[(df['n1'] == original_ndsi_pass1) & (df['n2'] == original_ndsi_pass2) & (df['r1'] == original_red_pass1) & (df['r2'] == original_red_pass2),'kappa'],data=df, color="red",size=7)
plt.xlabel('Red band Pass 1 (milli-reflectance)',size = label_size)
plt.ylabel('Kappa (%)',size = label_size)
plt.xticks(rotation=45, horizontalalignment='center',size = tick_size)
plt.yticks(size = tick_size)
ax.yaxis.labelpad = 7
#RED 2
ax = plt.subplot2grid((2,2), (1,1))
#plt.title('Effect of Red band pass 2 on Kappa score',size = 16)
sns.boxplot(x='r2',y='kappa',data=df, color="skyblue")
sns.swarmplot(x='r2',y=df.loc[(df['n1'] == original_ndsi_pass1) & (df['n2'] == original_ndsi_pass2) & (df['r1'] == original_red_pass1) & (df['r2'] == original_red_pass2),'kappa'],data=df, color="red",size=7)
plt.xlabel('Red band Pass 2 (milli-reflectance)',size = label_size)
plt.ylabel('Kappa (%)',size = label_size)
plt.xticks(rotation=45, horizontalalignment='center',size = tick_size)
plt.yticks(size = tick_size)
ax.yaxis.labelpad = 7
fig.set_size_inches(w=16,h=10)
st.set_y(0.95)
fig.subplots_adjust(top=0.85,wspace=0.3, hspace=0.4)
fig.savefig('boxplot_KAPPA_'+tile+'.png')
plt.close(fig)



#PLOT OA
fig = plt.figure()
#st = fig.suptitle("EFFECTS OF THRESHOLD PARAMETERS ON OVERALL ACCURACY",size = 18)
gridspec.GridSpec(2,2)
ax = plt.subplot2grid((2,2), (0,0))
#plt.title('Effect of NDSI pass 1 on OA',size = 16)
sns.boxplot(x='n1',y='OA',data=df, color="skyblue")
sns.swarmplot(x='n1',y=df.loc[(df['n1'] == original_ndsi_pass1) & (df['n2'] == original_ndsi_pass2) & (df['r1'] == original_red_pass1) & (df['r2'] == original_red_pass2),'OA'],data=df, color="red",size=7)
plt.xlabel('NDSI Pass 1',size = label_size)
plt.ylabel('OA (%)',size = label_size)
plt.xticks(rotation=45, horizontalalignment='center',size = tick_size)
plt.yticks(size = tick_size)
ax.yaxis.labelpad = 7
ax = plt.subplot2grid((2,2), (0,1))
#plt.title('Effect of NDSI pass 2 on OA',size = 16)
sns.boxplot(x='n2',y='OA',data=df, color="skyblue")
sns.swarmplot(x='n2',y=df.loc[(df['n1'] == original_ndsi_pass1) & (df['n2'] == original_ndsi_pass2) & (df['r1'] == original_red_pass1) & (df['r2'] == original_red_pass2),'OA'],data=df, color="red",size=7)
plt.xlabel('NDSI Pass 2',size = label_size)
plt.ylabel('OA (%)',size = label_size)
plt.xticks(rotation=45, horizontalalignment='center',size = tick_size)
plt.yticks(size = tick_size)
ax.yaxis.labelpad = 7
ax = plt.subplot2grid((2,2), (1,0))
#plt.title('Effect of Red band pass 1 on OA',size = 16)
sns.boxplot(x='r1',y='OA',data=df, color="skyblue")
sns.swarmplot(x='r1',y=df.loc[(df['n1'] == original_ndsi_pass1) & (df['n2'] == original_ndsi_pass2) & (df['r1'] == original_red_pass1) & (df['r2'] == original_red_pass2),'OA'],data=df, color="red",size=7)
plt.xlabel('Red band Pass 1 (milli-reflectance)',size = label_size)
plt.ylabel('OA (%)',size = label_size)
plt.xticks(rotation=45, horizontalalignment='center',size = tick_size)
plt.yticks(size = tick_size)
ax.yaxis.labelpad = 7
ax = plt.subplot2grid((2,2), (1,1))
#plt.title('Effect of Red band pass 2 on OA',size = 16)
sns.boxplot(x='r2',y='OA',data=df, color="skyblue")
sns.swarmplot(x='r2',y=df.loc[(df['n1'] == original_ndsi_pass1) & (df['n2'] == original_ndsi_pass2) & (df['r1'] == original_red_pass1) & (df['r2'] == original_red_pass2),'OA'],data=df, color="red",size=7)
plt.xlabel('Red band Pass 2 (milli-reflectance)',size = label_size)
plt.ylabel('OA (%)',size = label_size)
plt.xticks(rotation=45, horizontalalignment='center',size = tick_size)
plt.yticks(size = tick_size)
ax.yaxis.labelpad = 7
fig.set_size_inches(w=16,h=10)
st.set_y(0.95)
fig.subplots_adjust(top=0.85,wspace=0.3, hspace=0.4)
fig.savefig('boxplot_OA_'+tile+'.png')
plt.close(fig)



#PLOT F1 SCORE SNOW
fig = plt.figure()
#st = fig.suptitle("EFFECTS OF THRESHOLD PARAMETERS ON F1 SNOW SCORES",size = 18)
gridspec.GridSpec(2,2)
ax = plt.subplot2grid((2,2), (0,0))
#plt.title('Effect of NDSI pass 1 on F1 snow score',size = 16)
sns.boxplot(x='n1',y='F1_snow',data=df, color="skyblue")
sns.swarmplot(x='n1',y=df.loc[(df['n1'] == original_ndsi_pass1) & (df['n2'] == original_ndsi_pass2) & (df['r1'] == original_red_pass1) & (df['r2'] == original_red_pass2),'F1_snow'],data=df, color="red",size=7)
plt.xlabel('NDSI Pass 1',size = label_size)
plt.ylabel('F1 snow (%)',size = label_size)
plt.xticks(rotation=45, horizontalalignment='center',size = tick_size)
plt.yticks(size = tick_size)
ax.yaxis.labelpad = 7
ax = plt.subplot2grid((2,2), (0,1))
#plt.title('Effect of NDSI pass 2 on F1 snow score',size = 16)
sns.boxplot(x='n2',y='F1_snow',data=df, color="skyblue")
sns.swarmplot(x='n2',y=df.loc[(df['n1'] == original_ndsi_pass1) & (df['n2'] == original_ndsi_pass2) & (df['r1'] == original_red_pass1) & (df['r2'] == original_red_pass2),'F1_snow'],data=df, color="red",size=7)
plt.xlabel('NDSI Pass 2',size = label_size)
plt.ylabel('F1 snow (%)',size = label_size)
plt.xticks(rotation=45, horizontalalignment='center',size = tick_size)
plt.yticks(size = tick_size)
ax.yaxis.labelpad = 7
ax = plt.subplot2grid((2,2), (1,0))
#plt.title('Effect of Red band pass 1 on F1 snow score',size = 16)
sns.boxplot(x='r1',y='F1_snow',data=df, color="skyblue")
sns.swarmplot(x='r1',y=df.loc[(df['n1'] == original_ndsi_pass1) & (df['n2'] == original_ndsi_pass2) & (df['r1'] == original_red_pass1) & (df['r2'] == original_red_pass2),'F1_snow'],data=df, color="red",size=7)
plt.xlabel('Red band Pass 1 (milli-reflectance)',size = label_size)
plt.ylabel('F1 snow (%)',size = label_size)
plt.xticks(rotation=45, horizontalalignment='center',size = tick_size)
plt.yticks(size = tick_size)
ax.yaxis.labelpad = 7
ax = plt.subplot2grid((2,2), (1,1))
#plt.title('Effect of Red band pass 2 on F1 snow score',size = 16)
sns.boxplot(x='r2',y='F1_snow',data=df, color="skyblue")
sns.swarmplot(x='r2',y=df.loc[(df['n1'] == original_ndsi_pass1) & (df['n2'] == original_ndsi_pass2) & (df['r1'] == original_red_pass1) & (df['r2'] == original_red_pass2),'F1_snow'],data=df, color="red",size=7)
plt.xlabel('Red band Pass 2 (milli-reflectance)',size = label_size)
plt.ylabel('F1 snow (%)',size = label_size)
plt.xticks(rotation=45, horizontalalignment='center',size = tick_size)
plt.yticks(size = tick_size)
ax.yaxis.labelpad = 7
fig.set_size_inches(w=16,h=10)
st.set_y(0.95)
fig.subplots_adjust(top=0.85,wspace=0.3, hspace=0.4)
fig.savefig('boxplot_F1_snow_'+tile+'.png')
plt.close(fig)


#PLOT F1 SCORE LAND
fig = plt.figure()
#st = fig.suptitle("EFFECTS OF THRESHOLD PARAMETERS ON F1 LAND SCORES",size = 18)
gridspec.GridSpec(2,2)
ax = plt.subplot2grid((2,2), (0,0))
#plt.title('Effect of NDSI pass 1 on F1 land score',size = 16)
sns.boxplot(x='n1',y='F1_land',data=df, color="skyblue")
sns.swarmplot(x='n1',y=df.loc[(df['n1'] == original_ndsi_pass1) & (df['n2'] == original_ndsi_pass2) & (df['r1'] == original_red_pass1) & (df['r2'] == original_red_pass2),'F1_land'],data=df, color="red",size=7)
plt.xlabel('NDSI Pass 1',size = label_size)
plt.ylabel('F1 land (%)',size = label_size)
plt.xticks(rotation=45, horizontalalignment='center',size = tick_size)
plt.yticks(size = tick_size)
ax.yaxis.labelpad = 7
ax = plt.subplot2grid((2,2), (0,1))
#plt.title('Effect of NDSI pass 2 on F1 land score',size = 16)
sns.boxplot(x='n2',y='F1_land',data=df, color="skyblue")
sns.swarmplot(x='n2',y=df.loc[(df['n1'] == original_ndsi_pass1) & (df['n2'] == original_ndsi_pass2) & (df['r1'] == original_red_pass1) & (df['r2'] == original_red_pass2),'F1_land'],data=df, color="red",size=7)
plt.xlabel('NDSI Pass 2',size = label_size)
plt.ylabel('F1 land (%)',size = label_size)
plt.xticks(rotation=45, horizontalalignment='center',size = tick_size)
plt.yticks(size = tick_size)
ax.yaxis.labelpad = 7
ax = plt.subplot2grid((2,2), (1,0))
#plt.title('Effect of Red band pass 1 on F1 land score',size = 16)
sns.boxplot(x='r1',y='F1_land',data=df, color="skyblue")
sns.swarmplot(x='r1',y=df.loc[(df['n1'] == original_ndsi_pass1) & (df['n2'] == original_ndsi_pass2) & (df['r1'] == original_red_pass1) & (df['r2'] == original_red_pass2),'F1_land'],data=df, color="red",size=7)
plt.xlabel('Red band Pass 1 (milli-reflectance)',size = label_size)
plt.ylabel('F1 land (%)',size = label_size)
plt.xticks(rotation=45, horizontalalignment='center',size = tick_size)
plt.yticks(size = tick_size)
ax.yaxis.labelpad = 7
ax = plt.subplot2grid((2,2), (1,1))
#plt.title('Effect of Red band pass 2 on F1 land score',size = 16)
sns.boxplot(x='r2',y='F1_land',data=df, color="skyblue")
sns.swarmplot(x='r2',y=df.loc[(df['n1'] == original_ndsi_pass1) & (df['n2'] == original_ndsi_pass2) & (df['r1'] == original_red_pass1) & (df['r2'] == original_red_pass2),'F1_land'],data=df, color="red",size=7)
plt.xlabel('Red band Pass 2 (milli-reflectance)',size = label_size)
plt.ylabel('F1 land (%)',size = label_size)
plt.xticks(rotation=45, horizontalalignment='center',size = tick_size)
plt.yticks(size = tick_size)
ax.yaxis.labelpad = 7
fig.set_size_inches(w=16,h=10)
st.set_y(0.95)
fig.subplots_adjust(top=0.85,wspace=0.3, hspace=0.4)
fig.savefig('boxplot_F1_land_'+tile+'.png')
plt.close(fig)

#PLOT F1 SCORE CLOUD
fig = plt.figure()
#st = fig.suptitle("EFFECTS OF THRESHOLD PARAMETERS ON F1 CLOUD SCORES",size = 18)
gridspec.GridSpec(2,2)
ax = plt.subplot2grid((2,2), (0,0))
#plt.title('Effect of NDSI pass 1 on F1 cloud score',size = 16)
sns.boxplot(x='n1',y='F1_cloud',data=df, color="skyblue")
sns.swarmplot(x='n1',y=df.loc[(df['n1'] == original_ndsi_pass1) & (df['n2'] == original_ndsi_pass2) & (df['r1'] == original_red_pass1) & (df['r2'] == original_red_pass2),'F1_cloud'],data=df, color="red",size=7)
plt.xlabel('NDSI Pass 1',size = label_size)
plt.ylabel('F1 cloud (%)',size = label_size)
plt.xticks(rotation=45, horizontalalignment='center',size = tick_size)
plt.yticks(size = tick_size)
ax.yaxis.labelpad = 7
ax = plt.subplot2grid((2,2), (0,1))
#plt.title('Effect of NDSI pass 2 on F1 cloud score',size = 16)
sns.boxplot(x='n2',y='F1_cloud',data=df, color="skyblue")
sns.swarmplot(x='n2',y=df.loc[(df['n1'] == original_ndsi_pass1) & (df['n2'] == original_ndsi_pass2) & (df['r1'] == original_red_pass1) & (df['r2'] == original_red_pass2),'F1_cloud'],data=df, color="red",size=7)
plt.xlabel('NDSI Pass 2',size = label_size)
plt.ylabel('F1 cloud (%)',size = label_size)
plt.xticks(rotation=45, horizontalalignment='center',size = tick_size)
plt.yticks(size = tick_size)
ax.yaxis.labelpad = 7
ax = plt.subplot2grid((2,2), (1,0))
#plt.title('Effect of Red band pass 1 on F1 cloud score',size = 16)
sns.boxplot(x='r1',y='F1_cloud',data=df, color="skyblue")
sns.swarmplot(x='r1',y=df.loc[(df['n1'] == original_ndsi_pass1) & (df['n2'] == original_ndsi_pass2) & (df['r1'] == original_red_pass1) & (df['r2'] == original_red_pass2),'F1_cloud'],data=df, color="red",size=7)
plt.xlabel('Red band Pass 1 (milli-reflectance)',size = label_size)
plt.ylabel('F1 cloud (%)',size = label_size)
plt.xticks(rotation=45, horizontalalignment='center',size = tick_size)
plt.yticks(size = tick_size)
ax.yaxis.labelpad = 7
ax = plt.subplot2grid((2,2), (1,1))
#plt.title('Effect of Red band pass 2 on F1 cloud score',size = 16)
sns.boxplot(x='r2',y='F1_cloud',data=df, color="skyblue")
sns.swarmplot(x='r2',y=df.loc[(df['n1'] == original_ndsi_pass1) & (df['n2'] == original_ndsi_pass2) & (df['r1'] == original_red_pass1) & (df['r2'] == original_red_pass2),'F1_cloud'],data=df, color="red",size=7)
plt.xlabel('Red band Pass 2 (milli-reflectance)',size = label_size)
plt.ylabel('F1 cloud (%)',size = label_size)
plt.xticks(rotation=45, horizontalalignment='center',size = tick_size)
plt.yticks(size = tick_size)
ax.yaxis.labelpad = 7
fig.set_size_inches(w=16,h=10)
st.set_y(0.95)
fig.subplots_adjust(top=0.85,wspace=0.3, hspace=0.4)
fig.savefig('boxplot_F1_cloud_'+tile+'.png')
plt.close(fig)
