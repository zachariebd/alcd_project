
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun  8 17:43:30 2020

@author: zacharie
"""

import sys
import os
import errno
import re
from datetime import datetime, timedelta, date
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import shutil
import matplotlib.gridspec as gridspec
from matplotlib.colors import LogNorm
import random
from math import sqrt
from matplotlib.ticker import PercentFormatter
from matplotlib.ticker import (MultipleLocator, AutoMinorLocator)

dates = ["01/04/2019","28/05/2019","30/06/2019","20/07/2019","04/08/2019","03/10/2019","17/11/2019","24/12/2019","10/02/2020","18/03/2020"]

LIS_s= np.asarray([2.14,3.26,0.45,0.003,0.003,0.001,40.24,18.75,7.95,2.4]).flatten()
LIS_l = np.asarray([33.41,36.73,68.18,48.73,81.28,58.47,12.71,27.59,22.04,21.2]).flatten()
LIS_c = np.asarray([64.45,60.01,31.37,51.27,18.72,41.53,47.05,53.67,70.02,76.4]).flatten()

ALCD_s = np.asarray([5.22,5.41,0.62,0.016,0,0.04,40.05,22.44,15.21,12.88]).flatten()
ALCD_l= np.asarray([36.69,38.11,68.76,47.73,85.47,62.42,14.31,34.87,51.01,27.08]).flatten()
ALCD_c= np.asarray([58.09,56.48,30.61,52.25,14.53,37.54,45.63,42.69,33.78,60.04]).flatten()

nx = np.asarray([1,2,3,4,5,6,7,8,9,10]).flatten()

fig , ax = plt.subplots()

width = 0.3
ax.bar(nx-(0.6*width),LIS_l,width, label = "Land",color ='darkorange')
ax.bar(nx+(0.6*width),ALCD_l,width, label = "Land",color ='peru')
ax.bar(nx-(0.6*width),LIS_s,width, label = "Snow",color ='lawngreen',bottom=LIS_l)
ax.bar(nx+(0.6*width),ALCD_s,width, label = "Snow",color ='limegreen',bottom=ALCD_l)
ax.bar(nx-(0.6*width),LIS_c,width, label = "Cloud",color ='silver',bottom=LIS_s+LIS_l)
ax.bar(nx+(0.6*width),ALCD_c,width, label = "Cloud",color ='dimgray',bottom=ALCD_s+ALCD_l)


h, l = ax.get_legend_handles_labels()
ph = [plt.plot([],marker="", ls="")[0]]*2
handles = ph + h
labels = ["LIS:", "ALCD:"] + l
plt.legend(handles, labels,bbox_to_anchor=(0, 1, 1, 0), loc="lower left", mode="expand",  ncol=4,prop={'size': 12})

#plt.legend(bbox_to_anchor=(0, 1, 1, 0), loc="lower left", mode="expand", ncol=3)


#plt.legend(ncol=2,framealpha = 0.2,prop={'size': 9})
plt.ylabel('Percent of pixels %', size = 16)
plt.xlabel("Scenes", size = 16)
plt.xticks(ticks=nx,labels=dates,rotation = 45,ha="right",size=12)
plt.yticks(size=12)
#plt.xticklabels(dates,rotation = 45,ha="right")
#plt.yaxis.grid()
ax.yaxis.set_minor_locator(MultipleLocator(5))
ax.yaxis.grid(True,which='both')
ax.set_axisbelow(True)
fig.tight_layout(h_pad=-0.1)
plt.savefig('im_percent_px.png')

    











# fig, (ax0, ax1,ax2) = plt.subplots(nrows=3, sharex=True)

# label_size = 12
# width = 0.3
# ax0.bar(nx-(0.5*width),LIS_s,width, label = "LIS",color ='darkorange')
# ax0.bar(nx+(0.5*width),ALCD_s,width, label = "ALCD",color = 'darkviolet')
# ax0.legend(ncol=2,framealpha = 0.2,prop={'size': 9})
# ax0.set_ylabel('Percent of \nsnow pixels %', size = label_size)
# ax0.yaxis.grid()
# ax0.set_axisbelow(True)

# label_size = 12
# width = 0.3
# ax1.bar(nx-(0.5*width),LIS_l,width, label = "LIS",color ='darkorange')
# ax1.bar(nx+(0.5*width),ALCD_l,width, label = "ALCD",color = 'darkviolet')
# ax1.legend(ncol=2,framealpha = 0.2,prop={'size': 9})
# ax1.set_ylabel('Percent of \nland pixels %', size = label_size)
# ax1.yaxis.grid()
# ax1.set_axisbelow(True)

# label_size = 12
# width = 0.3
# ax2.bar(nx-(0.5*width),LIS_c,width, label = "LIS",color ='darkorange')
# ax2.bar(nx+(0.5*width),ALCD_c,width, label = "ALCD",color = 'darkviolet')
# ax2.legend(ncol=2,framealpha = 0.2,prop={'size': 9})
# ax2.set_ylabel('Percent of \ncloud pixels %', size = label_size)
# ax2.yaxis.grid()
# ax2.set_axisbelow(True)

# ax2.set_xlabel("Scenes", size = label_size)
# ax2.set_xticks(nx)
# ax2.set_xticklabels(dates,rotation = 45,ha="right")
# fig.align_ylabels()
# fig.tight_layout(h_pad=-0.1)
# fig.savefig('im_percent_px.png')
# plt.close(fig)









# width = 0.15
# ax1.bar(nx-(1.5*width),list_TP, width,label = "TP", align='center')
# ax1.bar(nx-(0.5*width),list_FN, width,label = "FN", align='center')
# ax1.bar(nx+(0.5*width),list_FP, width,label = "FP", align='center')
# ax1.bar(nx+(1.5*width),list_TN, width,label = "TN", align='center')
# ax1.set_yscale('log')
# ax1.legend(ncol=4,framealpha = 0.2,prop={'size': 9})
# ax1.set_ylabel('Points', size = label_size)
# #ax1.yaxis.set_major_locator(matplotlib.ticker.MaxNLocator(5)) 
# #order = int(math.ceil(math.log(max(list_total), 10)))
# order = int(math.ceil(math.log(101, 10)))
# ax1.set_yticks(10 ** np.arange(order+1))
# locmin = matplotlib.ticker.LogLocator(base=10.0,subs=(0.2,0.4,0.6,0.8),numticks=order + 1)
# ax1.yaxis.set_minor_locator(locmin)
# ax1.yaxis.set_minor_formatter(matplotlib.ticker.NullFormatter())
# ax1.yaxis.grid()
# ax1.set_axisbelow(True)

# width = 0.3
# ax2.bar(nx-(0.5*width),list_snow_true,width, label = "Snow",color ='b')
# ax2.bar(nx+(0.5*width),list_no_snow_true,width, label = "No Snow",color = 'g')
# #ax2.bar(nx,list_snow_true, label = "Snow (in situ)", align='center',bottom=list_no_snow_true,color ='b')
# order = int(math.ceil(math.log(max(list_total), 10)))
# ax2.set_yscale('log')

# ax2.set_yticks(10 ** np.arange(order+1))

# ax2.legend(ncol=2,framealpha = 0.2,prop={'size': 9})
# ax2.set_ylabel('Points', size = label_size)
# ax2.set_xlabel(xaxe_name + " "+ ext, size = label_size)
# ax2.set_xticks(nx)
# ax2.set_xticklabels(list_labels_box,rotation = label_orientation,ha="right")
# #ax2.yaxis.set_major_locator(matplotlib.ticker.MaxNLocator(3)) 
# #ax2.yaxis.set_minor_locator(matplotlib.ticker.MultipleLocator(5)) 
# #ax2.get_yaxis().get_major_formatter().labelOnlyBase = False
# #ax2.tick_params(axis='y', which='minor', bottom=False)
# #locmin = matplotlib.ticker.LogLocator(base=10.0,subs=(0.2,0.4,0.6,0.8),numticks=3)
# #ax2.yaxis.set_minor_locator(locmin)
# #ax2.yaxis.set_minor_formatter(matplotlib.ticker.NullFormatter())
# ax2.yaxis.grid()
# ax2.set_axisbelow(True)
# locmin = matplotlib.ticker.LogLocator(base=10.0,subs=(0.2,0.4,0.6,0.8),numticks=order+1)
# ax2.yaxis.set_minor_locator(locmin)
# ax2.yaxis.set_minor_formatter(matplotlib.ticker.NullFormatter())

# fig.align_ylabels()
# fig.tight_layout(h_pad=-0.1)
# fig.savefig(os.path.join(path_eval_dir,txt_v+'_COMBINE.png'))
# plt.close(fig)
    
    
    
